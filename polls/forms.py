from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from .models import Article, ArticleEval, Round, PhraseEval


class ArticleEvalForm(forms.ModelForm):

    class Meta:
        model = ArticleEval
        fields = ('is_credible',)


class PhraseEvalForm(forms.ModelForm):

    class Meta:
        model = PhraseEval
        fields = ('rate',)


class ArticleForm(forms.ModelForm):

    class Meta:
        model = Article
        fields = '__all__'


class RoundForm(forms.ModelForm):

    class Meta:
        model = Round
        fields = '__all__'

class UserCreateForm(UserCreationForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")

    def save(self, commit=True):
        user = super(UserCreateForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
        return user
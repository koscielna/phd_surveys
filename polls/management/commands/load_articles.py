import os
from django.core.management.base import BaseCommand, CommandError
from django.db.utils import IntegrityError
from polls.models import Article
from surveys.settings.base import BASE_DIR

class Command(BaseCommand):
    help = 'Loads articles to the database from "articles" directory.'

    def handle(self, *args, **options):
        ARTICLES_DIR = BASE_DIR + '/articles'
        for file_name in os.listdir(ARTICLES_DIR):
            if not file_name.startswith('.'):
                with open(os.path.join(ARTICLES_DIR, file_name)) as f:
                    try:
                        id = file_name.split('.')[0]
                        Article.objects.create(
                            id=int(id),
                            body=f.read()
                        )
                        print("Article " + id + " loaded to database.")
                    except IntegrityError:
                        pass

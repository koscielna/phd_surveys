import datetime
from django.db import models
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.utils import timezone
from django.contrib.auth.models import User


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)


@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()


# Articles and Phrases
class Round(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_date = models.DateTimeField(default=timezone.now)
    finished_date = models.DateTimeField(null=True)
    is_random = models.BooleanField(default=False)

    def __str__(self):
        isrand = 'Random round ' if self.is_random else 'Normal round '
        finisheddate = 'not finished' if not self.finished_date else self.finished_date.strftime('%d-%m-%Y %H:%M')

        return isrand + finisheddate


class Article(models.Model):
    body = models.CharField(max_length=40000)

    def __str__(self):
        return "Article " + str(self.pk)


class ArticleEval(models.Model):
    NOT_EVALUATED = -1
    NON_CREDIBLE = 0
    CREDIBLE = 1

    Rate = (
        (0, 'Artykuł jest niewiarygodny'),
        (1, 'Artykuł jest wiarygodny')
    )

    is_credible = models.IntegerField(choices=Rate)
    article = models.OneToOneField(Article, on_delete=models.CASCADE)
    round = models.OneToOneField(Round, on_delete=models.CASCADE, null=True)

class Phrase(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    body = models.CharField(max_length=4000)

    def __str__(self):
        return self.body

class PhraseEval(models.Model):
    NON_CREDIBLE = 0
    NEUTRAL = 1
    CREDIBLE = 2

    Rate = (
        (NON_CREDIBLE, 'Zdanie jest niewiarygodne'),
        (NEUTRAL, 'Zdanie jest neutralne'),
        (CREDIBLE, 'Zdanie jest wiarygodne'),
    )

    phrase = models.OneToOneField(Phrase, on_delete=models.CASCADE)
    rate = models.IntegerField(choices=Rate, blank=True, null=True)
    round = models.ForeignKey(Round, on_delete=models.CASCADE, null=True)


class RandomPhraseEval(models.Model):
    NONSENSE = -1
    NON_CREDIBLE = 0
    NEUTRAL = 1
    CREDIBLE = 2

    Rate = (
        (NONSENSE, 'Zdania nie można ocenić'),
        (NON_CREDIBLE, 'Zdanie jest niewiarygodne'),
        (NEUTRAL, 'Zdanie jest neutralne'),
        (CREDIBLE, 'Zdanie jest wiarygodne'),
    )

    phrase = models.ForeignKey(Phrase, on_delete=models.CASCADE)
    rate = models.IntegerField(choices=Rate, blank=True, null=True)
    round = models.ForeignKey(Round, on_delete=models.CASCADE, null=True)


@receiver(post_save, sender=Article)
def create_phrases_with_article(sender, instance, **kwargs):
    if kwargs.get('created', False):
        sentences = instance.body.split("\n")
        for sentence in sentences:
            if len(sentence) > 1:
                ph = Phrase.objects.create(article=instance, body=sentence)
                ph.save()
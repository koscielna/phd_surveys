# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-07-13 20:21
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0013_auto_20170601_1332'),
    ]

    operations = [
        migrations.AlterField(
            model_name='round',
            name='created_date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]

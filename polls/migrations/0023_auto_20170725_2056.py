# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-07-25 20:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0022_auto_20170725_2047'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='body',
            field=models.CharField(max_length=40000),
        ),
        migrations.AlterField(
            model_name='phrase',
            name='body',
            field=models.CharField(max_length=2000),
        ),
    ]

from django.conf.urls import url
from polls.views import round, random_round, article_new, process_normal_round_form, \
    process_random_round_form
from . import views

app_name = 'polls'
urlpatterns = [
    url(r'^$', views.profile, name='profile'),
    url(r'^round/(?P<round_id>[0-9]+)/$', round, name='round'),
    url(r'^process_normal_round_form/(?P<round_id>[0-9]+)/(?P<article_id>[0-9]+)/$',
        process_normal_round_form,
        name='process_normal_round_form'),
    url(r'^process_random_round_form/(?P<round_id>[0-9]+)/$',
        process_random_round_form,
        name='process_random_round_form'),
    url(r'^random_round/(?P<pk>[0-9]+)/$', random_round, name='random_round'),
    url(r'^article/new', article_new, name='article_new'),
]
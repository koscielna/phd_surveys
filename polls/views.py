import random
from itertools import chain

from datetime import datetime

from django.db import connection, IntegrityError
from django.contrib import messages
from django.forms import modelformset_factory, Select
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import redirect, render

from .forms import ArticleForm, ArticleEvalForm, UserCreateForm

from polls.models import Round, Article, Phrase, PhraseEval, RandomPhraseEval
from polls.email_list import email_list

def index(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse('polls:profile'))
    else:
        return HttpResponseRedirect(reverse('login'))


def delete_unfinished_rounds(request):
    unfinished_rounds = request.user.round_set.filter(finished_date__isnull=True)
    unfinished_rounds.delete()


def login_error(request):
    return HttpResponseRedirect(reverse('login'))


def signup(request):
    if request.method == 'POST':
        form = UserCreateForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data.get('email')
            if email in email_list:
                form.save()
                username = form.cleaned_data.get('username')
                raw_password = form.cleaned_data.get('password1')
                user = authenticate(username=username, password=raw_password, email=email)
                login(request, user)
                return redirect('polls:profile')
            else:
                messages.error(request, ("Nie można założyć konta. Skontaktuj się z osobą, od której \
                 otrzymałeś link do strony tej rejestracji, lub upewnij się, że wpisałeś poprawny adres email."))
                return redirect('signup')
    else:
        form = UserCreateForm()
    return render(request, 'polls/signup.html', {'form': form})


@login_required
def profile(request):
    delete_unfinished_rounds(request)
    user = request.user
    rounds = user.round_set.all()

    if request.method == 'POST':
        return choose_round(request)

    return render(request, 'polls/profile.html', {
        'rounds': rounds,
    })

def choose_round(request):
    all_noncred = get_phrases_with_evals(0)
    all_neu = get_phrases_with_evals(1)
    all_cred = get_phrases_with_evals(2)

    new_round = Round(
                 user=request.user,
                 created_date=timezone.now(),
                 is_random=random.getrandbits(1)
                )
    new_round.save()

    if len(all_noncred + all_neu + all_cred) == 0:
        new_round.is_random = 0
        new_round.save()
        return HttpResponseRedirect(reverse('polls:round', args=(new_round.id,)))
    elif new_round.is_random:
        return HttpResponseRedirect(reverse('polls:random_round', args=(new_round.id,)))
    else:
        return HttpResponseRedirect(reverse('polls:round', args=(new_round.id,)))


def get_random_article():
    # choose random article and pass it to the context
    ids_domain = Article.objects.filter(articleeval__isnull=True).values_list('id', flat=True)
    random_id = random.choice(ids_domain)
    article = Article.objects.get(pk=random_id)
    return article


def get_phrases_with_evals(cred_value):
    query = '''
        SELECT distinct phrase.id FROM polls_phrase AS phrase

        LEFT JOIN polls_phraseeval AS ph_eval ON phrase.id = ph_eval.phrase_id AND ph_eval.rate = %s
            LEFT JOIN polls_randomphraseeval AS r_ph_eval ON phrase.id = r_ph_eval.phrase_id

        WHERE r_ph_eval.id IS NULL and ph_eval.id IS NOT NULL;
    '''

    with connection.cursor() as cursor:
        cursor.execute(query, [cred_value])
        res = cursor.fetchall()
        result = []
        for row in res:
            result.append(row[0])

        return result


def get_random_phrases_set():
    all_noncredible_ph_ids = get_phrases_with_evals(0)
    all_neutral_ph_ids = get_phrases_with_evals(1)
    all_credible_ph_ids = get_phrases_with_evals(2)

    subset_len = 11

    try:
        subset_credible_ph_ids = random.sample(all_credible_ph_ids, subset_len)
    except ValueError:
        subset_credible_ph_ids = all_credible_ph_ids

    subset_credible_phrases = Phrase.objects.filter(id__in=subset_credible_ph_ids)

    try:
        subset_noncredible_ph_ids = random.sample(all_noncredible_ph_ids, subset_len)
    except ValueError:
        subset_noncredible_ph_ids = all_noncredible_ph_ids

    subset_noncredible_phrases = Phrase.objects.filter(id__in=subset_noncredible_ph_ids)

    try:
        subset_neutral_ph_ids = random.sample(all_neutral_ph_ids, subset_len)
    except ValueError:
        subset_neutral_ph_ids = all_neutral_ph_ids

    subset_neutral_phrases = Phrase.objects.filter(id__in=subset_neutral_ph_ids)

    random_phrases = list(chain(subset_credible_phrases, subset_noncredible_phrases, subset_neutral_phrases))

    return random_phrases


@login_required
def round(request, round_id, **kwargs):
    survey_round = Round.objects.get(pk=round_id)

    try:
        article = get_random_article()
    except IndexError:
        survey_round.is_random = 1
        survey_round.save()
        return HttpResponseRedirect(reverse('polls:random_round', args=(survey_round.id,)))


    phrases = article.phrase_set.all()
    art_eval_form = ArticleEvalForm(request.POST or None)

    try:
        for phrase in phrases:
            PhraseEval.objects.create(phrase=phrase, rate=None, round=survey_round)
    except IntegrityError:
        pass

    phrase_eval_queryset = survey_round.phraseeval_set.all()
    PhraseEvalFormSet = modelformset_factory(PhraseEval, fields=('rate',),
                                             widgets={'rate': Select(attrs={'required': True})},
                                             max_num=len(phrases)-1
                                             )
    formset = PhraseEvalFormSet(request.POST or None,
                                queryset=phrase_eval_queryset,
                                initial=[{'rate': None}]
                                )

    return render(request, 'polls/round.html', context={
        'round': survey_round,
        'article': article,
        'art_eval_form': art_eval_form,
        'formset': formset
    })

def process_normal_round_form(request, round_id, article_id):
    survey_round = Round.objects.get(pk=round_id)
    article = Article.objects.get(pk=article_id)

    phrase_eval_queryset = survey_round.phraseeval_set.all()
    PhraseEvalFormSet = modelformset_factory(PhraseEval, fields=('rate',),
                                             widgets={'rate': Select(attrs={'required': True})},
                                             max_num=len(phrase_eval_queryset) - 1
                                             )
    formset = PhraseEvalFormSet(request.POST or None, queryset=phrase_eval_queryset, initial=[{'rate': None}])
    art_eval_form = ArticleEvalForm(request.POST or None)

    if formset.is_valid() and art_eval_form.is_valid():
        for form in formset:
            form.cleaned_data['id'].rate = form.cleaned_data['rate']
            form.cleaned_data['id'].save()

        art_eval = art_eval_form.save(commit=False)
        art_eval.round = survey_round
        art_eval.article = article
        art_eval.save()

        survey_round.finished_date = datetime.now()
        for ph_ev in survey_round.phraseeval_set.all():
            if ph_ev.rate == None:
                ph_ev.delete()
        survey_round.save()

        messages.success(request, 'Dziękujemy za wypełnienie ankiety!')
        return HttpResponseRedirect(reverse('polls:profile'))
    else:
        messages.error(request, formset.errors)
        return HttpResponseRedirect(reverse('polls:profile'))


@login_required
def random_round(request, **kwargs):
    survey_round = Round.objects.get(pk=kwargs['pk'])
    phrases = get_random_phrases_set()

    if len(phrases) == 0:
        messages.error(request,
                       'Wygląda na to, że wszystkie zdania mają już oceny. Dziękujemy za udział w badaniu!')
        return HttpResponseRedirect(reverse('polls:profile'))


    for phrase in phrases:
        RandomPhraseEval.objects.create(phrase=phrase, rate=None, round=survey_round)

    phrase_eval_queryset = survey_round.randomphraseeval_set.all()
    PhraseEvalFormSet = modelformset_factory(RandomPhraseEval, fields=('rate',),
                                             widgets={'rate': Select(attrs={'required': True})},
                                             max_num=len(phrases)-1
                                             )
    formset = PhraseEvalFormSet(request.POST or None, queryset=phrase_eval_queryset, initial=[{'rate': None}])

    return render(request, 'polls/random_round.html', {
        'round': survey_round,
        'formset': formset
    })


def process_random_round_form(request, round_id):
    survey_round = Round.objects.get(pk=round_id)
    phrase_eval_queryset = survey_round.randomphraseeval_set.all()
    PhraseEvalFormSet = modelformset_factory(RandomPhraseEval, fields=('rate',),
                                             widgets={'rate': Select(attrs={'required': True})},
                                             max_num=len(phrase_eval_queryset)-1
                                             )
    formset = PhraseEvalFormSet(request.POST or None, queryset=phrase_eval_queryset, initial=[{'rate': None}])

    if formset.is_valid():
        for form in formset:
            form.cleaned_data['id'].rate = form.cleaned_data['rate']
            form.cleaned_data['id'].save()

        survey_round.finished_date = datetime.now()
        for ph_ev in survey_round.randomphraseeval_set.all():
            if ph_ev.rate == None:
                ph_ev.delete()
        survey_round.save()

        messages.success(request, 'Dziękujemy za wypełnienie ankiety!')
        return HttpResponseRedirect(reverse('polls:profile'))
    else:
        messages.error(request, formset.errors)
        return HttpResponseRedirect(reverse('polls:profile'))


@user_passes_test(lambda u: u.is_superuser)
def article_new(request):
    form = ArticleForm()
    return render(request, 'polls/article_edit.html', {'form': form})
$("#toggle-instruction").click(function(){
    var toggleHeight = $("#upper-instruction").outerHeight() == 410 ? "0px" : "410px";
    $("#upper-instruction").animate({ height: toggleHeight });

    $(this).text(function(i, text){
          return text === "SCHOWAJ INSTRUKCJĘ" ? "POKAŻ INSTRUKCJĘ" : "SCHOWAJ INSTRUKCJĘ";
      })
});
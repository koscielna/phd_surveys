from django import forms
from django.contrib import admin

from .models import Article, Phrase, Round, PhraseEval, ArticleEval, RandomPhraseEval

admin.site.register(Article)
admin.site.register(Phrase)
admin.site.register(Round)
admin.site.register(PhraseEval)
admin.site.register(RandomPhraseEval)
admin.site.register(ArticleEval)

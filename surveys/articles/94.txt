 Wiele ludzi stosuje żele antybakteryjne do rąk w celu usunięcia drobnoustrojów w postaci bakterii, archeonów, pierwotniaków, grzybów.
 Chcemy tak zdezynfekować ręce, aby po dotknięciu ust, żadne drobnoustroje nie przedostały się do organizmu.
 Sprawdźmy… jak działają żele antybakteryjne? Według badań przeprowadzonych przez naukowców z University of Missouri i opublikowanych w internetowym czasopiśmie naukowym PLOS ONE, żele antybakteryjne do rąk powodują wchłanianie 100 razy więcej Bisfenolu A (BPA)  zaburzającego pracę hormonów i wywołującego wiele chorób, jak nowotwory.
 Związek ten stosowany jest przy produkcji nie tylko tworzyw sztucznych.
 Wywołuje liczne choroby na tyle groźne, że nawet amerykańskie koncerny przemysłu tworzyw sztucznych postanowiły wycofać BPA z procesu produkcji opakowań do przechowywania żywności.
 „Nasze badania wykazały, że duże ilości BPA mogą przenosić się na ręce, a następnie do żywności, którą dotykamy i spożywamy, a także wchłaniać się przez skórę do wnętrza organizmu”– powiedział autor Frederick vom Saal.
 Badania wykazały już wcześniej, że żele antybakteryjne do rąk powodują wchłanianie wyższych poziomów chemikaliów.
 Poziomy są tak wysokie, że mogą powodować niebezpieczne choroby.
 „BPA wykazało właściwości hormonopodobne i okazały się być przyczyną wad rozwoju płodu, niemowląt, dzieci i dorosłych.
 Może także powodować raka, problemy metaboliczne i odpornościowe u gryzoni.
 BPA z papierów termotransferowych wchłania się szybko do krwi, na tych poziomach wzrasta wiele chorób, takich jak cukrzyca i otyłość” – powiedział vom Saal.
 BPA powoduje choroby serca i niepłodność Przeprowadzono badanie, w którym uczestnicy mieli trzymać w dłoniach papierowe rachunki/paragony.
 Niektórzy uczestnicy skorzystali z żelu antybakteryjnego do rąk przed przystąpieniem do eksperymentu, a niektórzy zjedli frytki rękami po trzymaniu papierowego rachunku.
 Poziomy BPA we krwi, skórze i moczu uczestników mierzono przed i po każdym doświadczeniu.
 Naukowcy odkryli, że trzymanie papieru rachunkowego przez 45 sekund powoduje wzrost poziomu BPA na skórze do 581 μg – 40 procent tego wzrostu wystąpiło zaledwie w ciągu pierwszych 2 sekund.
 Po około 4 minutach poziomy chemikaliów na skórze spadły o 27 procent, prawdopodobnie dlatego, że BPA wchłaniania się w miarę upływu czasu.
 90 minut po otrzymaniu paragonu, uczestnik miał poziom krwi i moczu o stężeniu 20 mg BPA / g kreatyniny, które wcześniej związane były ze zwiększonym ryzykiem wystąpienia choroby serca i cukrzycy typu 2.
 Zaobserwowane poziomy uważane są również za wystarczająco wysokie, aby zwiększyć ryzyko rozwoju zaburzeń u dzieci.
 Jakie były poziomy chemikaliów u osób, które zdezynfekowały ręce żelem antybakteryjnym? Mieli poziomy krwi i moczu 100 razy wyższe, niż osoby, które nie stosowały żelów.
 Poziomy BPA wzrosły bardziej u kobiet, niż u mężczyzn.
 Odkrycia te są szczególnie niepokojące dla osób zajmujących się rachunkami/fakturami/paragonami sklepowymi przez cały dzień w pracy.
 „Rachunki ze sklepu czy restauracji, bilety lotnicze, pokwitowania bankomatów i inne papiery termiczne wykorzystują ogromne ilości BPA na powierzchni papieru jako wspomagacz dla farby.
 Problem polega na tym, że my, jako konsumenci mamy żele antybakteryjne do rąk, kremy do rąk, mydła i osłony przeciwsłoneczne na naszych dłoniach, które drastycznie zmieniają (zwiększają) stopień absorpcji BPA znaleziony na tych rachunkach” – powiedział vom Saal.
 Naukowcy zauważyli, że ludzie, którzy trzymają paragony, a potem jedzą jedzenie, są narażeni na BPA dwa razy: przez skórę i przez usta.
 Dlatego naukowcy zalecają, aby nie jeść natychmiast po napromieniowaniu termicznym (dotykaniu lub trzymaniu paragonów).
 Żele antybakteryjne do rąk były wcześniej powiązane z innymi problemami zdrowotnymi, w tym zaburzeniami endokrynologicznymi, zaburzeniami odporności i rakiem.
 Badania wykazały również, że przyczyniają się do rozwoju bakterii opornych na antybiotyki i powodują nierównowagę bakteryjną na skórze – a może nawet zwiększają ryzyko różnych infekcji.
 W ostatnich latach zwrócono większą uwagę na niebezpieczeństwa związane z BPA.
 Ważne jest, aby zapamiętać, że związek ten znajduje się się nie tylko na paragonach, fakturach, biletach lotniczych, pokwitowaniach z bankomatu, ale również w plastikach na żywność i w puszkach z jedzeniem.

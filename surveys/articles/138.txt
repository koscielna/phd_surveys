 Witamina C jest związkiem chemicznym niezbędnym do życia.
 Ze wszystkich istot żywych jedynie świnka morska i człowiek nie są w stanie sami jej syntetyzować.
 Dlatego też jest ona dla nas substancją egzogenną, przyjmowaną ze środowiska zewnętrznego.
 By lepiej zobrazować jak bardzo i w jakich ilościach jest ona potrzebna, wystarczy wspomnieć, że na przykład pies ważący 30 kg syntetyzuje jej około 25 g dziennie.
 Dlatego też może się wydać zaskakujące, że Światowa Organizacja Zdrowia rekomenduje spożywanie witaminy C w ilości od 45 do 90 mg na dobę dla dorosłego, w pełni zdrowego człowieka ważącego przeciętnie 75 kg.
 Wielu uznanych lekarzy uważa, że wpływ witaminy C na nasz organizm jest niedoceniany, a wręcz ignorowany.
 Wielkimi propagatorami suplementacji i terapii dużymi dawkami witaminy C był Linus Pauling, naukowiec i dwukrotny noblista jak również Albert Szent-Györgyi, który za prace na temat witaminy C został wyróżniony nagrodą Nobla.
 Linus Pauling wraz z Ewanem Cameronem przeprowadzili badania na około 100 pacjentach w terminalnym stadium choroby nowotworowej.
 Eksperyment polegał na podawaniu chorym 10 g witaminy C dziennie.
 Ta grupa chorych przeżyła średnio 10 miesięcy dłużej niż pacjenci, którym witaminy C nie podawano.
 Po roku, żyło 22% leczonych wysokimi dawkami witaminy C a pacjentów bez tej suplementacji tylko 0,4%.
 Przez ponad 2 lata żyło średnio około 20 pacjentów, a 8 z nich żyło nadal w momencie publikowania badania.
 Na tej podstawie Narodowy Instytut Raka w Stanach Zjednoczonych przeprowadził własne badania kliniczne nad wpływem wit. C.
 Niestety w badaniach tych Instytut nie zastosował formy dożylnej witaminy a jedynie jej formę doustną.
 Jak wiadomo okres półtrwania witaminy C wynosi około pół godziny, więc nie ma możliwości, by przy takiej formie podaży utrzymać poziom terapeutyczny.
 Co więcej, tak wysokie, lecznicze stężenie witaminy C we krwi musi utrzymywać się przez dłuższy czas.
 Dlatego też badanie przeprowadzone przez Narodowy Instytut Raka nie wykazało jej działania terapeutycznego.
 Ostatnio jednak pojawiły się głosy, że dyrektor oddziału alternatywnych terapii nowotworowych Jeffrey White, w oparciu o najnowsze wyniki badań udowadniające nie tylko skuteczność, ale też tłumaczące cały mechanizm tej metody, zabiega o ponowne przeanalizowanie tej terapii.
 Nie ma natomiast najmniejszych wątpliwości, że każdy lek, czy ten na receptę, czy ten powszechnie dostępny nawet na stacjach benzynowych, posiadają całą listę skutków ubocznych, niejednokrotnie dłuższą niż potencjalnych korzyści.
 Natomiast witamina C skutków ubocznych prawie nie ma.
 Prawie, gdyż może się zdarzyć, że przy nasyceniu organizmu witaminą C pojawi się lekka biegunka lub wzdęcia.
 W leczeniu stosuje się dawki nieco niższe od tych, które wywołałyby biegunkę.
 Jednak u każdego dawka ta jest inna.
 Ocenia się, że u tej samej osoby dawka witaminy C, która nie wywoła biegunki jest 10-krotnie wyższa w sytuacji choroby.
 Na przykład jeśli u osoby zdrowej podanie doustnie 2 g witaminy C wywołuje biegunkę to w trakcie infekcji bakteryjnej podanie 20 g nie wywoła biegunki.
 Poza tym nie udowodniono żadnych z kiedyś przypisywanych witaminie C, skutków ubocznych, takich jak tworzenie kamieni nerkowych, obniżania poziomu witaminy B12 czy erozji szkliwa.
 Zastosowanie Dr Robert Cathcart, ekspert w dziedzinie leczenia witaminą C, leczący kilkanaście tysięcy pacjentów, nigdy nie opisał żadnego, innego od biegunki, skutku ubocznego.
 Opracował on również konkretne dawki i częstotliwość ich podawania.
 I tak na przykład przy przeziębieniu zaleca 30-40 g/dobę w 6-10 dawkach, a w reumatoidalnym zapaleniu stawów 15-100 g/dobę w 4-15 dawkach.
 Wielu lekarzy, stosujący witaminoterapię w swojej codziennej praktyce, uważa, że stosowanie dużych, gramowych dawek witaminy C jest znacznie mniej niebezpieczne niż powikłania związane ze stosowaniem innych konwencjonalnych leków.
 Poza tym w leczeniu dożylnym (stosowanym zwykle w terapii, a nie suplementacji) stosuje się askorbinian sodu, nie zaś kwas askorbinowy, gdyż jest on lepiej tolerowany w dużych dawkach.
 Udowodniono wiele korzyści z długoterminowego suplementowania witaminy C w ilości 2-3 g/dobę, takich jak: zmniejszenie ryzyka raka chorób układu krążenia zaćmy wzrost frakcji HDL cholesterolu (tzw. ”dobrego”) zmniejszeniem frakcji LDL cholesterolu (tzw. ”złego”) obniżenie ciśnienia krwi zmniejszenie ryzyka chorób sercowo-naczyniowych.
 Lekarzem szeroko stosującym witaminę C był dr Frederick R. Klenner.
 Leczył skutecznie choroby bakteryjne i wirusowe takie jak ospa, odra, świnka, tężec czy polio.
 Uważał on, że dopóki białe krwinki nie zostaną wysycone witaminą C, nie są w stanie spełniać swojej roli.
 Zadaniem kwasu askorbinowego jest łączenie się z białkową otoczką wirusa (kapsydem), hamowanie jego rozwoju a stopień neutralizacji drobnoustroju jest proporcjonalny do stężenia i jego czasu trwania.
 Doktor Klenner nie wykluczał możliwości podawania antybiotyku równocześnie z witaminą C w przypadku przedłużających się infekcji.
 Stosował ją również w zatruciach tlenkiem węgla, ołowiem oraz użądleniach owadów, reakcjach alergicznych i infekcjach grzybiczych.
 Witamina C a choroba nowotworowa Witamina C jest z powodzeniem stosowana w leczeniu onkologicznym.
 Już na początku terapii pacjenci odczuwają znaczną redukcję bólu, który prawie zawsze towarzyszy chorobie nowotworowej oraz poprawę komfortu życia, poprzez wzmocnienie organizmu do walki z chorobą.
 Jednak na tym nie koniec.
 Pacjenci otrzymujący wysokie dożylne dawki żyją znacznie dłużej niż przewidywana przed terapią długość życia.
 Istniej wiele badań wskazujących, że taka terapia hamuje rozwój komórek rakowych.
 Poza zwiększeniem odpowiedzi immunologicznej organizmu, prawdopodobnie odpowiada za to powstawanie wody utlenionej w komórkach jako wynik końcowego etapu metabolizmu witaminy C.
 Niszczy ona wybiórczo komórki nowotworowe w tzw. reakcji Fentona.
 Woda utleniona w połączeniu z jonami żelaza tworzy wolne rodniki.
 W wyniku powstałego stresu oksydacyjnego komórka rakowa ginie.
 Powstało wiele prac naukowych, które pokazują, że warto włączyć witaminoterapię do tradycyjnych metod leczenia raka, jakimi są radio- i chemioterapia.
 Witamina C zwiększa efektywność działania powyższych metod, co więcej chroni ona zdrowe komórki przed skutkami ubocznymi ich działania.
 Większość lekarzy zajmujących się leczeniem integracyjnym chorób nowotworowych zaleca dożylne podawanie wysokich dawek askorbinianu sodu i codzienne uzupełnianie doustne formą liposomalną witaminy C przez co najmniej rok.
 Ostateczny plan leczenia, dawkowanie i zalecane zabiegi zawsze jednak pozostaje do decyzji lekarza prowadzącego.

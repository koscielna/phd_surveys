  Łódzcy naukowcy opracowali metodę termicznego niszczenia rozsianych komórek nowotworowych z użyciem nanozasobników ferromagnetyka.
 Dzięki tej metodzie możliwe będzie niszczenie tylko komórek rakowych, nie uszkadzając zdrowych komórek- poinformowała politechnika Łódzka.
 Przy zastosowaniu tej metody przepływające w układzie krwionośnym nanorurki odnajdują guza nowotworowego zawierającego zdegenerowane komórki i się do nich przyczepiają.
 Napromieniowanie tkanki falą elektromagnetyczną powoduje nagrzanie komórek nowotworowych powyżej temperatury ich apoptozy wywołując nekrozę, czyli śmierć komórek - mówił PAP współtwórca rozwiązania prof. Zbigniew Kołaciński z Instytutu Mechatroniki i Systemów Informatycznych Politechniki Łódzkiej.
 Opracowanie metody niszczenie rozsianych komórek nowotworowych jelita grubego z użyciem nanocząstek to rezultat projektu sponsorowanego przez NCBiR, zrealizowanego w Politechnice Łódzkiej, z udziałem Uniwersytetu Medycznego w Łodzi oraz firmy AMEPOX.
 Prof. Kołaciński podkreślił, że naukowcy zajmujący się nanotechnologią wychodzą naprzeciw niedoskonałościom obecnie stosowanych metod leczenia raka - leczenia chirurgicznego, w przypadku którego pacjenci zgłaszają się do lekarza zbyt późno, dlatego często jest to leczenie jedynie paliatywne, czyli przeciwbólowe, a także chemioterapii i radioterapii, których stosowanie niszczy niestety także komórki zdrowe.
 „Powstała wśród naukowców, którzy zajmują się nanotechnologią chęć wprowadzenia nanocząstek do likwidacji komórek rakowych.
 Te nanocząstki muszą być zasobnikami leków lub innych elementów, które potrafią zniszczyć komórki rakowe” - wyjaśnił naukowiec.
 Na Politechnice Łódzkiej opracowano metodę otrzymywania takich nanozasobników poprzez syntezę nanorurek węglowych różnymi metodami: łukową, metodą plazmy mikrofalowej oraz chemicznego osadzania z fazy gazowej.
 Nanorurki zawierają w sobie ferromagnetyk np. żelazo, które można następnie rozgrzać przy pomocy fali elektromagnetycznej.
 „Jeżeli taki zasobnik wprowadzimy do organizmu i przyłączymy go do komórek rakowych spełni on rolę mikro źródła grzejnego, które zniszczy te komórki.
 Jest to zniszczenie całkowite, bo jeżeli temperatura komórki nowotworowej przekroczy 42 stopni C następuje jej nekroza” - podkreślił prof. Kołaciński.
 Nanozasobniki biegną w układzie krwionośnym i odnajdują chore komórki - uruchamiany jest wówczas proces termoablacji tzn. nagrzewania polem elektromagnetycznym.
 „Chore komórki podlegają napromieniowaniu falą elektromagnetyczną o wysokiej częstotliwości radiowej, która podgrzewa żelazo w procesie hipertermii niszczącej komórki rakowe” - dodał naukowiec.
 Do tego celu łódzcy naukowcy wykorzystują generatory o częstotliwości radiowej pracujące w zakresie od setek kiloherców do rzędu kilkudziesięciu megaherców.
 „Opracowaliśmy urządzenie do hipertermii, czyli nagrzewania komórek falą elektromagnetyczną, które wraz z patentem adresującym zasobniki do komórek rakowych, tworzy całość, która likwiduje wyłącznie zwyrodniałe komórki” - podkreślił prof. Kołaciński.
 Opracowana przez łódzkich naukowców metoda została przebadana na komórkach nowotworowych jelita grubego.
 Konieczne są jeszcze testy na zwierzętach i testy kliniczne.
 W końcowej fazie wdrożenia klinicznego projektu planuje się umieszczenie całego człowieka w urządzeniu emitującym kontrolowane dawki pola elektromagnetycznego o częstotliwości radiowej.
 Prof. Kołaciński liczy, że za 10 lat ta metoda ta będzie skutecznie stosowana w określonych typach nowotworów.
 „Na pewno nie do wszystkich typów, natomiast w określonych typach nowotworów zostanie sprawdzona i będzie mogła być stosowana” - ocenił naukowiec.

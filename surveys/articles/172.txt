 W 2016 r. ponad 23 tys. rodziców nie zdecydowało się na szczepienia ochronne swoich dzieci.
 Czy grozi nam epidemia? Z reguły zaczyna się od przypadkowej rozmowy.
 Znajoma mówi, że nie zaszczepiła swego nowo narodzonego dziecka.
 W szpitalu po porodzie lekarze próbują wyperswadować pomysł nieszczepienia, ale udaje się wyjść ze szpitala bez żadnej szczepionki.
 Antyszczepionkowcy z reguły nie zapisują dzieci do państwowych przychodni, leczą je prywatnie i - rzecz jasna - nie szczepią.
 Izabela Filc-Redlińska w książce "Szczepionki.
 Nie daj się zwariować" zauważa, że najczęściej nie szczepią mieszkańcy miast (najwięcej dzieci nieszczepionych mieszka w województwach śląskim, wielkopolskim i mazowieckim).
 Raczej są to osoby zamożne.
 Im osoba bogatsza, tym mniejsza szansa, że wykupi szczepienia zalecane.
 Co grozi rodzicom lub prawnym opiekunom za niezaszczepienie dziecka? Według rozporządzenia Ministra Zdrowia najbardziej oporni będą zmuszeni do zapłacenia kary.
 Mimo to, z roku na rok, przybywa odmów zaszczepienia dzieci.
 Choroba lepsza od powikłań poszczepiennych? W 2002 r. odnotowano 2,2 tys. odmów zaszczepienia dzieci, w 2004 r. – 3,5 tys., w 2012 r. – 5,3 tys., a w 2013 r. – 7,2 tys.
 W 2014 roku padł kolejny „rekord”, 12 361 maluchów nie otrzymało obowiązkowych szczepionek.
 Specjaliści zaczęli mówić o problemie z nasilającym się ruchem antyszczepionkowym.
 W 2015 r. nastąpił kolejny spory skok.
 Według najnowszych danych z Narodowego Instytutu Zdrowia Publicznego – Państwowego Zakładu Higieny, w 2015 r. odmów było już 16 689, a w minionym 2016 r. odmów szczepień było już 23 147.
 Jak wynika z liczb, z roku na rok, systematycznie przybywa rodzin, które świadomie rezygnują z obowiązkowych szczepień (za NIZP-PZH).
 Dane te trzeba jednak interpretować z głową.
 Dotyczą one zarówno dzieci w ogóle nieszczepionych, jak i tych, które nie przeszły tylko wybranych szczepień obowiązkowych.
 W praktyce część maluchów otrzymuje niektóre szczepienia, inne są wykonywane z opóźnieniem, co nie powoduje usunięcia wcześniejszej odmowy ze statystyk.
 Należy też zaznaczyć, że powyższych danych nie należy mylić z liczbą niezaszczepionych dzieci.
 Być może rodzice mogli wielokrotnie odmawiać zaszczepienia jednego dziecka.
 Czego najbardziej boją się rodzice decydujący się na nieszczepienie dzieci? Głównie groźnych, ich zdaniem powikłań poszczepiennych oraz tego, że już małym dzieciom podaje się wiele szczepionek w krótkich odstępach czasu.
 Wielu z nich uważa też, że szczepienie może spowodować u ich maluchów alergiczne reakcje.
 Na guru ruchu antyszczepionkowego wyrasta prof. Maria Dorota Majewska, neurobiolog, która przez 25 lat pracowała w USA.
 List otwarty, który skierowała do Polskiego Towarzystwa Wakcynologii oraz Głównego Polskiego Towarzystwa Pediatrycznego jest od lat powielany przez internautów, rodzice przekazują go sobie jak łańcuszek, to manifestem ruchu antyszczepionkowego.
 - Po przeczytaniu listu włos się zaczyna człowiekowi jeżyć.
 Skala kryminalnego oszustwa, które - bodaj kosztem życia i zdrowia milionów ludzi - ma napędzić wybranym kolejne miliardy dochodu, jest po prostu przerażająca.
 Życzymy pani prof. Majewskiej, aby nic jej się nie stało - tak zareagował na list profesorki przerażony internauta na swojej stronie.
 Od lat ruch antyszczepionkowy rośnie w siłę, w internecie mnożą się strony z protestami przeciw obowiązkowym szczepieniom, na Facebooku bardzo prężnie działa strona STOP NOP.
 Na ich profilu (ponad 53 tysiące obserwatorów) można przeczytać m.in. o mafii szczepionkowej w Polsce lub o diecie, która może być alternatywą dla szczepień.
 W portalu YouTube można znaleźć materiał wideo z zapisem akcji rozdawania ulotek uświadamiających zagrożenia wynikające ze stosowania szczepionek.
 Według badań przeprowadzonych przez GIS (Główny Inspektor Sanitarny) zaledwie 2 proc. ankietowanych przyznało, że powodem dla których nie chcą poddać się szczepieniom są obawy związane z wystąpieniem w przeszłości u siebie lub u dziecka niepożądanego odczynu poszczepiennego.
 Niewiele mniej osób wskazało odrębność kulturową, religijną lub etniczną.
 Natomiast aż 28 proc. jako przyczynę nieszczepienia podało wpływ ruchów antyszczepionkowych i ta grupa między początkiem a końcem roku 2014 powiększyła się najbardziej, bo aż o 66 proc.
 Czy grozi nam epidemia? Eksperci uspokajają, że ruch antyszczepionkowy w Polsce wciąż nie jest tak silny jak w krajach Europy Zachodniej czy w USA.
 Wystarczy jednak zajrzeć do lektury forów internetowych i blogów, by widzieć, że przeciwników szczepień wciąż przybywa, a ruch nie słabnie.
 W Polsce zaszczepialność obowiązkowymi szczepionkami jest nadal bardzo wysoka, wynosi ponad 95 proc., a barierą bezpieczeństwa jest szczepienie nie mniej niż 95 proc. dzieci.
 Jeśli jednak w każdym roczniku nie zaszczepi się co dziesiąte dziecko, to już po pięciu latach będzie w Polsce aż 20 tys. dzieci podatnych na zakażenie.
 A to wystarczy, żeby wybuchła mimiepidemia np. odry czy krztuśca.
 Miniepidemie np. odry wybuchają w krajach w których poziom wyszczepienia ludności spada poniżej 90 proc.
 Według WHO odra pozostaje jedną z głównych przyczyn śmierci małych dzieci na świecie.
 W 2013 roku zmarło na nią 145 700 osób - w większości dzieci poniżej 5. roku życia.
 Zaniechanie lub ograniczenie masowości szczepień, do czego dążą ruchy antyszczepionkowe, może doprowadzić do powrotu groźnych chorób, o których myślimy obecnie tylko w aspekcie historycznym.
 W nieodległej przeszłości, w latach 1993-1996 w sąsiadujących z nami krajach powstałych po rozpadzie ZSSR, doszło do epidemii błonicy, odnotowano tysiące przypadków zachorowań pośród niezaszczepionych dzieci i dorosłych, którzy nie otrzymali dawek przypominających szczepionek oraz bardzo wysoką 10 proc. śmiertelność.
 Akcja "Szczepienia chronią" 26 kwietnia rozpoczął się Europejski Tydzień Szczepień 2017.Celem tej corocznej kampanii jest zwrócenie uwagi i zwiększenie świadomości tego, jak ważne i potrzebne są szczepienia.
 Europejski Tydzień Szczepień to inicjatywa Światowej Organizacji Zdrowia (WHO) realizowana i koordynowana przez państwa na poziomie lokalnym.
 W tym roku Europejski Tydzień Szczepień zwraca szczególną uwagę na potrzeby i korzyści wynikające ze szczepień na każdym etapie życia.
 Hasło przewodnie tegorocznych obchodów to "Szczepienia Chronią".
 Więcej informacji o Europejskim Tygodniu Szczepień 2017 na stronach WHO oraz GIS (akcja informacyjna „Zaszczep w sobie chęć szczepienia”).

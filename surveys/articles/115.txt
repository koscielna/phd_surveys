 Nie wierzymy w szczepienia... i umieramy po grypie — tak urzekająco zatytułowany jest artykuł w „Gazecie Wyborczej".
 Nie da się ukryć, sezon grypowy zainaugurowano w tym roku ostro.
 Zazwyczaj zaczynało się łagodniej.
 Przynajmniej nie straszono nas śmiercią już na wstępie.
 Rytuał był niezmienny — w studiach telewizyjnych pojawiali się  medialni eksperci.
 W trosce o nasze dobro przestrzegali przed zbliżającą się falą grypy i tradycyjnie zapewniali, że w tym razem to już na pewno czeka nas wyjątkowo groźna epidemia.
 Szczepionka przeciwko grypie to fantastyczny produkt.
 Oczywiście z punktu widzenia marketingu.
 Przeznaczona jest dla wszystkich, w dodatku należałoby ją powtarzać co roku.
 Specjaliści od marketingu przekonują nas też, że jeśli wątpimy w oficjalną wersję korzyści płynących z tej  szczepionki, to jesteśmy cywilizacyjnie zapóźnieni.
 Ciemnogród, po prostu.
 A jednak trudno do Ciemnogrodu zaliczyć Cochrane Collaboration.
 To ciesząca się ogromnym prestiżem międzynarodowa organizacja non profit, kierowana przez naukowców najwyższej klasy.
 W sprawie szczepionek na grypę i ich skuteczności nie szczędzi sarkastycznych uwag — z żadnych badań klinicznych nie wynika na przykład związek między szczepieniami a mniejszą liczbą powikłań pogrypowych, wymagających hospitalizacji.
 Ja swoich dzieci nie szczepię przeciwko grypie.
 Dlaczego? Bo to nie ma najmniejszego sensu.
 Ale wie pani doskonale, że tej wypowiedzi nie autoryzuję — wyznał mi kiedyś pewien autorytet od szczepionek .
 Pan doktor w sezonie grypowym praktycznie nie opuszcza kanap telewizji śniadaniowych, zachęcając widzów do szczepień.
 Swoich dzieci nie szczepi, bo choć oficjalnie mówi coś innego , to prywatnie wierzy właśnie wnioskom naukowców z Cochrane Collaboration.
 A oni stwierdzili, że skuteczność szczepienia małych dzieci jest równa skuteczności placebo.
 Pewne tematy związane ze szczepionką na grypę są tabu.
 Ból głowy, pocenie się, ból mięśni, ból stawów.
 Gorączka, złe samopoczucie, dreszcze, zmęczenie.
 To częste objawy.
 Rzadko należy się liczyć z zaburzeniami czynności nerek, drgawkami połączonymi z gorączką, sztywnością karku i zaburzeniami neurologicznymi.
 Jeśli ktoś myśli, że to opis wyjątkowo paskudnej grypy, to się myli.
 To skutki uboczne, które -zgodnie z załączoną ulotką — mogą wystąpić po przyjęciu popularnej szczepionki.
 Czy słyszeli Państwo o skutkach ubocznych od jakiegoś medialnego apostoła szczepień ? Nie ma o nich ani słowa również w artykule w „GW".
 Nie ma tam też wzmianki, że szczepionka ma działać na grypę wywołaną wirusami influenzy typu A i B, a miażdżąca większość zachorowań w tzw. sezonie grypowym to infekcje grypopodobne.
 Artykuł w „GW” wygląda jak przepisany z materiałów podrzuconych przez usłużnego piarowca z koncernu farmaceutycznego.
 Kończy się lakonicznym zdaniem, „Szczepionka kosztuje 20 — 40 złotych”.
 Któż byłby takim sknerą, by wybierać  umieranie po grypie...

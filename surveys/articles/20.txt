 ﻿Nasza wiedza na temat leków przeciwbólowych jest powierzchowna, oparta na niedoskonałych źródłach informacji.
 Właśnie z tego powodu możemy być dla siebie niebezpieczni, wybierając nieświadomie lek zgodnie z zasadą na dwoje babka wróżyła - mówi neurolog, dr n. med. Monika Białecka.
 Jak zatem korzystać z leków, żeby sobie nie szkodzić? Zarówno paracetamol jak i alkohol przetwarzane są w wątrobie przez te same enzymy.
 Jeśli zastosujemy preparat z paracetamolem, kiedy w naszym organizmie znajduje się jeszcze wysokie stężenie alkoholu, dodatkowo obciążamy wątrobę i możemy doprowadzić do jej uszkodzenia przez toksyczne metabolity leku.
 W tym wypadku lepiej byłoby sięgnąć po preparat np. z ibuprofenem.
 Wiele osób podaje dzieciom leki przeciwbólowe przeznaczone wyłącznie dla dorosłych.
 Jest to błąd, ponieważ leki dla dorosłych różnią się substancją czynną i jej dawką zawartą w leku.
 Może się okazać, że lek, który podamy dziecku zawiera zbyt dużą dawkę substancji leczniczej, co może prowadzić do przedawkowania i wystąpienia u dziecka objawów zatrucia, niekiedy groźnych dla jego życia.
 Niektóre leki np. aspiryna jest przeciwwskazana w młodszej grupie wiekowej (poniżej 12 lat) ze względu na ryzyko wystąpienia uszkodzenia wątroby i mózgu.
 Ponadto u dziecka przy bólu i gorączce najczęściej występuje stan zapalny, więc warto się upewnić czy podawany lek ma działanie przeciwzapalne (tak jak np. ibuprofen).
 Często zdarza się, że przekonani o bezpieczeństwie leków przeciwbólowych, zażywamy je przy każdej możliwej okazji, nie przestrzegając zaleceń zawartych w ulotce.
 A nadużywanie leków przeciwbólowych może prowadzić do szeregu działań niepożądanych, takich jak nudności, choroba wrzodowa żołądka, zmiany skórne.
 Paradoksalnie, stosowanie leków przeciwbólowych kilka razy w tygodniu może być przyczyną polekowych bólów głowy tzw. bólów głowy z odbicia.
 Lek przeciwbólowy bez recepty jest środkiem doraźnej pomocy.
 Jeśli dolegliwości bólowe utrzymują się dłużej niż 3 dni konieczna jest wizyta u lekarza, a "maskowanie" problemu lekami przeciwbólowymi może tylko bardziej narażać nas na poważne konsekwencje zdrowotne.
 Żaden lek nie jest w pełni bezpieczny - bezpieczne jest tylko stosowanie leku zgodnie ze wskazaniami i w odpowiedniej dawce.
 Przykładowo wyjątkowo niebezpieczne może być nadużywanie uchodzącego za "bezpieczny" paracetamolu.
 Może to prowadzić np. do uszkodzenia wątroby.
 Przyjmowanie leku w dawkach większych aniżeli polecane w ulotce jest często bezcelowe czy wręcz niebezpieczne ze względu na tzw. "efekt pułapowy".
 Efekt pułapowy polega na braku silniejszego działania przeciwbólowego po przekroczeniu tzw. dawki maksymalnej, przyjmowanej jednorazowo lub w ciągu całej doby.
 W efekcie nie czujemy silniejszego działania przeciwbólowego, a zwiększa się ryzyko wystąpienia skutków niepożądanych.
 Popularna aspiryna, ibuprofen czy naproksen należą do grupy niesteroidowych leków przeciwzapalnych (NLPZ).
 Jednoczesne przyjmowanie dwóch leków z tej grupy jest błędem, gdyż zwiększa to ryzyko wystąpienia działań niepożądanych (np.: zaburzeń w obrębie przewodu pokarmowego).
 Dopuszczalne jest natomiast połączenie leku z grupy NLPZ z paracetamolem, który nie zalicza się do NLPZ.
 Powinniśmy zachować ostrożność także gdy łączymy leki przeciwbólowe np. z lekami na przeziębienie.
 Często zawierają one tę samą substancję czynną, co może zwiększać ryzyko przedawkowania leku i nasilać jego działania niepożądane.
 Jeśli mamy jakiekolwiek wątpliwości czy dane leki możemy stosować łącznie, powinniśmy zapytać lekarza lub farmaceuty.

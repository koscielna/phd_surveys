 Pojęcia wody „żywej” oraz wody „martwej” obrosły legendą.
 Te zjawiska mają wiele zastosowań w medycynie niekonwencjonalnej.
 Czym się charakteryzują i dlaczego uważane są za lecznicze? Woda „żywa” i „martwa” wytwarzane są za pomocą procesu elektrolizy.
 Elektroliza jest określana, jako różnorodne zjawiska występujące w warunkach przepływu prądu przez tak zwaną celę elektrolityczną.
 Taka cela składa się z: naczynia zawierającego roztwór elektrolitu, zanurzonej elektrody.
 Elektroda powinna być podłączona do zewnętrznego obwodu, które jest źródłem prądu.
 Wśród procesów można wyróżnić trzy główne: procesy transportu jonów do elektrody, proces przejścia ładunku między elektrodami, a roztworem, reakcje chemiczne zachodzące w trakcie, a także po przejściu ładunku.
 Proces elektrolizy wody jest odwracalny.
 Zjawisko wody „żywej” i „martwej” odkryto w 1965 roku w Instytucie Złóż Naftowych w Taszkiencie.
 Podczas badań nad elektrolizą wody zaobserwowano, że roztwór dzieli się na dwa rodzaje.
 Odkrytą wodę kwaśną i alkaliczną.
 Pierwszą z nich nazwano wodą „martwą”, zaś druga nosiła nazwę „żywej”.
 Dokonano wielu doświadczeń na obu roztworach.
 Najsłynniejszym z nich była próba wyhodowania bawełny.
 Podlewana wodą „martwą” nie rosła.
 Przy wielokrotnym polewaniu ziemi (w której były nasiona) wodą alkaliczną roślina zaczęła szybko rosnąć i zakwitać.
 Te dwa rodzaje wody wytwarzane są głównie w celach leczniczych, które rzekomo zostały potwierdzone licznymi przypadkami cudownych uleczeń ciężkich chorób.
 Ta metoda nie jest jednak akceptowana przez medycynę konwencjonalną.
 Specjaliści radzą pić lub nacierać się wodą „martwą” i „żywą” w zależności od dolegliwości.
 Odradza się regularne stosowanie przez długi czas.
 Według użytkowników wystarczy kilka dni, by efekty były widoczne.
 Obie wody mogą być podawane z ziołami.
 Mogą tez być podgrzewane.
 Żywa woda To woda o odczynie zasadowym.
 Wytwarzana jest poprzez zastosowanie katody.
 Charakteryzuje się białawą, mleczną barwą i odczynem pH przekraczającym 10.
 Legenda „żywej wody” znana jest od pokoleń i występuje w wielu przypowieściach, bajkach, pojawia się nawet w Biblii.
 Była w nich metaforą różnych zjawisk, jednak zawsze występowała, jako element pozytywny dający życie, zdrowie i nadludzkie siły.
 To przekonanie przetrwało do dzisiejszych czasów.
 Wiele osób stosuje „żywą” wodę zamiast konwencjonalnych leków.
 Eksperci w dziedzinie leczenia „żywą” wodą ostrzegają, że nie działa we wszystkich przypadkach, a do tego wymaga zastosowania specjalistycznej diety.
 Inaczej taka terapia nie przyniesie oczekiwanych skutków.
 Za sprawą elektrolizy reagują substancje takie jak: magnez, potas, żelazo, wapń, sód.
 W wodzie tworzą się wodorotlenki, które są odpowiedzialne za powstawanie osadu oraz zmętnienie.
 Woda nabiera też charakterystycznego mydlanego smaku i zapachu.
 Woda „żywa” może poprawić samopoczucie, przyspiesza gojenie ran, Woda tego typu zwiększa ciśnienie krwi.
 Jest zalecana w stanie ogólnego osłabienia i zmęczenia.
 Martwa woda To rodzaj wody o odczynie kwaśnym powyżej 4.
 Wytwarzana jest przy użyciu anody.
 Jest bogata w tlen i charakteryzuje się złotawym zabarwieniem.
 Woda ta posiada kwaśny posmak i ostry zapach, ma też bąbelki.
 Woda „martwa” według wielu źródeł opisujących jej działanie jest: bakteriobójcza, ma działanie przeciwgrzybiczne, przeciwgnilne, przeciwobrzękowe i przeciwzapalne.
 Jak się je wytwarza? Czysta woda jest bezwonna, nie ma smaku, ani barwy.
 Jej odczyn jest obojętny i nie przewodzi prądu.
 Ludzie korzystają zazwyczaj z wody pochodzącej z wodociągu lub własnych ujęć.
 Taka ciecz zawsze zawiera jakieś substancje i mikroorganizmy z grupy elektrolitów.
 Woda zyskuje więc tym samym zdolność przewodzenia prądu.
 Aby uzyskać wodę „żywą” i „martwą” należy zastosować elektrolizę.
 Urządzenie do wytwarzania takiego procesu można wykonać samodzielnie lub zakupić.
 Pod wpływem zachodzących reakcji chemicznych podczas elektrolizy w oddzielnych pojemnikach powstają oba rodzaje wody.
 Na wielu stronach można znaleźć szczegółowe instrukcje dotyczące produkcji i samodzielnego wykonania urządzenia.
 Na rynku są dostępne jonizatory wody, dzięki którym uzyskamy pożądany rodzaj wody.
 Ich ceny wahają się od dwóch do nawet sześciu tysięcy.
 Jakie jest zastosowanie? Wodę „martwą” i „żywą” stosuje się w różnych stężeniach, przez różny okres w przypadku dolegliwości takich jak: angina, depresja, problemy z ciśnieniem krwi, bóle głowy, bóle menstruacyjne, zapalenia gardła, bóle zębów, stany lękowe, zapalenie pęcherza moczowego, grypa, hemoroidy, problemy układu pokarmowego, liszaje, obrzęki, oparzenia, bóle stawowe, trądzik, żylaki, problemy z odżywianiem i brakiem minerałów.
 Podsumowanie Niektórzy wierzą w cudowną moc obu wód i twierdzą, że warto je wytwarzać.
 Inni odradzają takie zabiegi.
 Samodzielne wytwarzanie wody „żywej” i „martwej” może być niebezpieczne i doprowadzić do porażenia prądem.
 Przeciwnicy twierdzą, że oba rodzaje wody można uzyskać przy zastosowaniu już gotowych substancji, które wystarczy wymieszać.
 Na przykład wodę „żywą” można uzyskać dodając do szklanki wody pochodzącej z wodociągu niewielką ilość sody oczyszczonej.
 Stwierdzono, że obie wody mogą przynosić doraźną ulgę w bólu i tymczasową poprawę stanu zdrowia.
 Naukowcy twierdzą, że da się to wytłumaczyć, ponieważ substancje zawarte w wodzie mogą mieć działanie lecznicze.
 Problem jednak mogą stanowić inne produkty utleniania, które bywają toksyczne.
 Nie wiadomo, z czego dokładnie wynika tak wielkie zainteresowanie ludzi alternatywnymi metodami leczenia.
 Być może chodzi o małe zaufanie do medycyny konwencjonalnej? A może ludzie chcą odnaleźć bardziej ekologiczne rozwiązanie problemów zdrowotnych? Na stronach internetowych można odnaleźć wiele pozytywnych opinii i pochwał.
 Niestety zdarzają się też przypadki, kiedy ludzie przestają zażywać środki lecznicze przepisane przez lekarzy, a leczą się tylko i wyłącznie wodą.
 Wedy dochodzi do znacznego pogorszenia stanu zdrowia, a nawet zgonów.

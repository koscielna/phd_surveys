 Czy wiesz o tym, że woda jest jedyną substancją na Ziemi, której niezwykłych właściwości wciąż nie rozumiemy? Prowadzone na całym świecie badania zdołały jedynie uchylić rąbek jej tajemnicy.
 Jednak to co już odkryto zachęca do refleksji nad naszym życiem i zdrowiem.
 Woda z chemicznego punktu widzenia to tlenek wodoru wyrażony wzorem H2O i nie byłoby w tym nic nadzwyczajnego, gdyby nie okazało się, że jej struktura kryje w sobie znacznie więcej.
 Naukowcy od lat usiłują rozwikłać tajemnicę jej niezwykłych właściwości fizycznych i chemicznych.
 Jest to bowiem jedyna substancja na Ziemi, której objętość zwiększa się przy ujemnej temperaturze, a zmniejsza przy dodatniej.
 Wszystkie inne substancje reagują odwrotnie.
 Woda jako jedyna występuje w trzech stanach skupienia (ciecz, gaz i ciało stałe).
 W roztworach wodnych ma także największe napięcie powierzchniowe.
 Jest też najsilniejszym rozpuszczalnikiem jaki istnieje.
 Każda jej właściwość jest unikalna.1 Nie ma innej cząsteczki, która miałaby tyle anomalii.
 Chociaż wiemy jakie są właściwości wody to nadal nie rozumiemy dlaczego tak się dzieje.
 Badacze naukowi z różnych stron świata wciąż próbują dowiedzieć się w jaki sposób woda pnie się do góry w kapilarach wysokich drzew zwiększając swoje ciśnienie do kilkudziesięciu atmosfer.
 Dlaczego wykazuje właściwości innego związku chemicznego, chociaż jej skład nie ulega zmianie? Nie musi mieć przy tym bezpośredniego kontaktu z tą substancją, wystarczy, że znajduje się ona w jej otoczeniu.
 Dotychczas panowało przekonanie, że ważny jest skład chemiczny wody.
 Okazuje się jednak, że jej struktura jest znacznie ważniejsza.
 Naukowcy próbują zrozumieć działanie klastrów wody (grup cząstek tworzących struktury), którym przypisują właściwości odpowiedzialne za tzw. pamięć wody.
 Jak podkreśla dr Vladimir Voeikov2, dr nauk i prof. biologi Uniwersytetu Moskiewskiego – Zrobiliśmy ogromny krok naprzód.
 Zrozumieliśmy, że o wodzie nie wiemy prawie nic.
 To ogromny krok, bo jeśli czegoś nie wiemy zaraz chcemy się dowiedzieć.
 Dlaczego badania nad właściwościami wody są tak istotne? Zarówno człowiek jak i cała nasza planeta w ok. 70% składa się z wody (w niektórych miejscach jest jej więcej, a w innych mniej).
 Woda towarzyszy nam od początku naszego istnienia, od rozwoju płodowego i w różnych formach służy nam przez całe nasze życie.
 Bez niej by nas nie było, nie byłoby także innych form życia na Ziemi.
 Przeprowadzonych zostało wiele eksperymentów – sprawdzano jak wpływa na wodę pole elektryczne, magnetyczne, różne obiekty, a także ludzka obecność i emocje.
 Ku zaskoczeniu naukowców okazało się, że ludzkie pozytywne i negatywne emocje działają najsilniej.
 Znanym na całym świecie badaczem tego zjawiska jest dr Emoto Masaru3.
 W jego laboratorium woda poddawana jest działaniom konkretnych czynników, emocji lub słów, a następnie zamrażana w komorze kriogenicznej.
 Później pod mikroskopem obserwuje się budowę kryształów wody.
 Dr Emoto Masaru przeprowadził jeszcze inny ciekawy eksperyment.
 Trzy pojemniki napełnił ryżem i zalał go wodą.
 Przez cały miesiąc pierwszemu z nich mówił „dziękuję”, drugiemu „jesteś głupi”, a trzeci ignorował.
 Po upływie miesiąca ryż z pierwszego pojemnika zaczął fermentować wydzielając przyjemny zapach, ryż w drugim pojemniku sczerniał, a trzeci ignorowany zaczął gnić.
 Eksperyment ten był wiele razy powtarzany przez innych badaczy oraz osoby chcące na własne oczy przekonać się o jego skuteczności4.
 Efekt był analogiczny.
 Najwięcej szkód przynosi obojętność i negatywne emocje.
 Wyniki są nie tylko zaskakujące, ale dają także do myślenia.
 Jeżeli nasze ciało w większości składa się z wody to jak duży wpływ na nas ma nasze otoczenie i nasze własne myśli? W poprzednich artykułach prezentowałam badania dotyczące siły umysłu i jego oddziaływania na ciało (zapoznaj się proszę także z nimi).
 Natomiast niezwykłe właściwości wody stawiają je w zupełnie nowym świetle.
 Naukowcy poddali badaniom także wodę święconą badając jej kryształy po zamrożeniu w komorze kriogenicznej.
 Kryształy wody układały się w regularne sześcioramienne gwiazdy.
 Podczas gdy woda z próbki kontrolnej miała kształt nieregularnej plamy.
 Jak taka woda wpływa na nasze zdrowie? Wykazano to badając wpływ wody o zmienionej strukturze na ludzką krew.
 Lekarz medycyny Perl Laperla pobiera próbkę krwi z palca pacjenta i pod mikroskopem obserwuje sklejone czerwone ciałka krwi.
 Utraciły one ładunek elektryczny dlatego nastąpiła aglutynacja prowadząca do powstania skrzepów zagrażających zdrowiu.
 Następnie pacjentowi podawana jest do wypicia szklanka wody o zmienionej strukturze.
 Po 12 min badanie krwi jest powtarzane.
 Jej obraz jest już zupełnie inny.
 Komórki odzyskały ładunek elektryczny i zaczęły się od siebie odpychać – to pozwala im przenosić tlen.
 Ciekawym aspektem wydaje się efekt rozcieńczenia, ponieważ duże rozcieńczenie bardziej wpływa na pamięć wody niż małe.
 Efekt ten choć wciąż nie zrozumiały dla nauki jest wykorzystywany przez medycynę w postaci lekarstw homeopatycznych5.
 Jak zatem woda oczyszcza się z pamięci strukturalnej jeśli nie poprzez rozcieńczenie? Okazuje się, że dzieje się to poprzez zmianę jej stanu skupienia, gdy paruje i spada jako deszcz lub zamarza, a następnie topnieje.
 Wielokrotnie powtarzane badania wykazały także, że woda podzielona na dwa naczynia potrafi przekazywać sobie informacje.
 Podczas gdy woda w jednym naczyniu poddawana jest zmianom strukturalnym, woda w drugim oddalonym naczyniu przejmuje jej właściwości.
 Nie wiadomo dlaczego tak się dzieje.
 Czy to oznacza, że pomiędzy ludźmi składającymi się przecież w większości z wody istnieje związek na odległość? W 2005 roku prof. Vyacheslav Zvonnikov wraz z grupą pracowników przeprowadził eksperyment.
 Dwie osoby zostały umieszczone w odległości 15tys.
 kilometrów od siebie.
 Przed wykonaniem eksperymentu nie było, żadnej widocznej więzi między nimi.
 Zapisywane były najmniejsze zmiany – pozy, puls, częstotliwość oddechu, kardiogram i encefalogram.
 Eksperyment wykazał, że osoby te w jakiś sposób nastroiły się na te same fale.
 Aparatura zarejestrowała synchronizację poszczególnych części mózgu, oddechu, pulsu.
 Jak to wyjaśnić? Niestety wciąż nie ma odpowiedzi na to pytanie.
 Naukowcy podejrzewają, że woda wypełniająca organizm pełni funkcję swego rodzaju przekaźnika.
 Wiemy, że ludzki mózg składa się w ok 80% z wody.
 Jeżeli struktura wody przechowuje informacje, to czy stanowi ona główny element naszej pamięci? Jeśli pijemy wodę niosącą ze sobą informacje, czy nasze ciało je przyswaja? Badacze twierdzą, że jest to bardzo prawdopodobne.
 Dlatego właśnie warto zwrócić większą uwagę na badania nad fenomenem pamięci strukturalnej wody.
 Więcej informacji na temat wspomnianych badań oraz innych ciekawych właściwości wody znajdziesz w poniższym filmie dokumentalnym Saidy Medvedeva i Vasilija Anisimova  pt. „Woda”.
 Wystąpili w nim lekarze i naukowcy z różnych części świata m.in. Rosji, Stanów Zjednoczonych, Niemiec i Japonii.
 Chociaż film wzbudził kontrowersje w rosyjskim środowisku naukowym otrzymał Laur oraz trzy nagrody TEFI.

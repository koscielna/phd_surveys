 Jak uniknąć grypy i przeziębienia? W sezonie jesienno-zimowym ryzyko infekcji dróg oddechowych przenoszonych drogą kropelkową gwałtownie wzrasta.
 Każde kichnięcie czy kaszel może być początkiem uciążliwych dolegliwości, które mogą zmienić nasze plany.
 Warto zrobić jak najwięcej, aby ten sezon minął bez zdrowotnych niespodzianek.
 Zaszczep siebie i bliskich przeciwko grypie Celem szczepień przeciwko grypie jest nie tylko uniknięcie zachorowania, ale przede wszystkim uniknięcie poważnych powikłań pogrypowych takich, jak zapalenie płuc czy zapalenie mięśnia sercowego.
 Po szczepieniu układ odpornościowy potrzebuje od 6 do 8 tygodni na produkcję przeciwciał przeciwko wirusowi grypy.
 Dzieci i osoby starsze należą do grup ryzyka, dla których grypa jest bardzo poważnym zagrożeniem.
 Jedna dawka szczepionki wystarczy, aby zmniejszyć prawdopodobieństwo zachorowania u siebie i swoich najbliższych, a tym samym zarażenia osób w naszym otoczeniu.
 Pamiętajmy też, że szczepionka przeciwko grypie nie gwarantuje ochrony przed przeziębieniem, gdyż wywołujący je wirus różni się od wirusa grypy.
 Regularna i urozmaicona dieta, sen i aktywność fizyczna wzmocnią Twoją odporność Do sezonu infekcyjnego przygotowujemy się przez cały rok.
 Nasz styl życia ma ogromny wpływ na funkcjonowanie układu odpornościowego.
 Ten precyzyjny mechanizm obrony przed drobnoustrojami chorobotwórczymi do poprawnej pracy potrzebuje Twojej pomocy.
 Staraj się sypiać nie mniej niż 7-8 godzin na dobę.
 Odżywiaj się regularnie i zadbaj, aby Twoja dieta była bogata w warzywa i owoce z dużą zawartością witaminy C.
 Znajdziesz ją m.in. papryce, warzywach z zielonymi liśćmi, owocach kiwi, porzeczkach, malinach, żurawinie, jabłkach czy owocach cytrusowe.
 Pamiętaj o aktywności fizycznej, zwłaszcza na świeżym powietrzu - spacery, bieganie czy jazda na rowerze usprawniają Twój organizm.
 Zadbaj o wymianę powietrza w pomieszczeniach W sezonie jesienno-zimowym wilgotność jest znaczenie mniejsza.
 Zimne powietrze i centralne ogrzewanie powodują wysuszanie śluzówek w nosie, jamie ustnej i w okolicach oczu.
 To natomiast ułatwia wnikanie do organizmu różnego rodzaju wirusów i bakterii wywołujących infekcje.
 Poprzez częste wietrzenie pomieszczeń i stosowanie odpowiednich nawilżaczy zadbasz o właściwy poziom wilgotności tam, gdzie przebywasz najczęściej.
 Nie zarażaj innych.
 Gdy rozpoznasz objawy przeziębienia pozostań w domu Szczepionka przeciwko grypie chroni nie tylko Ciebie, ale także osoby z Twojego najbliższego otoczenia.
 Największe ryzyko infekcji występuje w miejscach dużych skupisk ludzi - w przedszkolach, szkołach czy środkach publicznej komunikacji.
 Dlatego w momencie pojawienia się objawów warto unikać kontaktów z bliskimi i z otoczeniem, aby nie szerzyć zakażenia.
 Dopóki występują objawy - możemy zarażać innych! Używanie chusteczek higienicznych chroni przed rozprzestrzenianiem się drobnoustrojów drogą kropelkową Zawsze miej w pogotowiu jednorazowe chusteczki higieniczne.
 Kiedy kaszlesz czy kichasz zasłaniaj nimi nos i usta.
 Chusteczek używaj tylko jeden raz i wyrzucaj po zużyciu.
 To kluczowe zasady higieny w czasie infekcji.
 Zmniejszają one ryzyko zarażania innych ludzi w domu i miejscach publicznych.
 Wirus grypy i wirusy wywołujące przeziębienie przedostają się do otoczenia drogą kropelkową m.in. gdy chory kaszle lub kicha.
 Poza organizmem człowieka mogą pozostać aktywne nawet kilka godzin.
 Zasłaniając usta i nos jednorazową chusteczką ograniczamy rozprzestrzenianie się chorobotwórczych cząsteczek.
 Zwykła chusteczka może ochronić inne osoby przed zarażeniem.
 Dokładne mycie rąk zmniejsza prawdopodobieństwo wystąpienia infekcji Częste mycie rąk jest najlepszym sposobem na pozbycie się drobnoustrojów.
 Aby skutecznie je usunąć nie wystarczy błyskawiczne opłukanie rąk wodą.
 Mycie mydłem powinno trwać co najmniej 20 sekund.
 Badania wykazują, że większość z nas myje ręce zbyt krótko i mało dokładnie.
 Warto poświęcić na tę czynność nieco więcej czasu i uwagi.
 Lecz objawy przeziębienia Nieleczone objawy przeziębienia (katar, gorączka, kaszel) utrudniają układowi odpornościowemu pokonanie infekcji.
 Katar może prowadzić do powikłań w postaci zapalenia zatok przynosowych.
 Szczególnie dbaj o drożność przewodów nosowych.
 Wysoka gorączka utrudnia prawidłowe funkcjonowanie organizmu.
 Staraj się jak najszybciej ją obniżyć.
 Kaszel pomaga w usuwaniu wydzieliny z dróg oddechowych, ale tylko wtedy, gdy jest to tzw. kaszel mokry.
 Ból gardła świadczy natomiast o stanie zapalnym, z którego może rozwinąć się infekcja bakteryjna.
 Pomocne mogą okazać się odpowiednie pastylki do ssania lub inhalatory działające przeciwbakteryjnie.
 W razie jakichkolwiek wątpliwości zwróć się do lekarza lub farmaceuty, który zaleci odpowiednie leki w bezpiecznych dawkach.
 Samodzielne kupowanie preparatów przeciwko objawom przeziębienia i grypy może doprowadzić do kumulacji danego leku w organizmie.
 Zwróć szczególną uwagę na to, aby nie łączyć leków o podobnym działaniu.
 Jeśli podejrzewasz grypę lub gdy przeziębienie nie mija po 3-4 dniach - koniecznie skonsultuj się z lekarzem Naucz się odróżniać przeziębienie od grypy i nie lekceważ jej.
 Grypa zaczyna się nagle -zaledwie w kilka godzin pojawia się gorączka powyżej 38-39 stopni C, kaszel, uczucie silnego rozbicia, ból głowy i mięśni.
 Przeziębienie rozpoczyna się łagodniej.
 W ciągu kilku dni następuje pogorszenie samopoczucia, następnie pojawia się katar i drapanie w gardle.
 Organizm potrafi zazwyczaj samodzielnie poradzić sobie z przeziębieniem.
 Jeśli po kilku dniach nie czujesz się lepiej i pojawiły się nowe objawy, koniecznie skonsultuj się z lekarzem.

 Mało kto wie, że w latach 60-tych Rząd rosyjski zabronił sprzedaży  mikrofalówek na terenie całego ZSRR.
 Od tamtych czasów zasada działania kuchenek mikrofalowych się nie zmieniła.
 Więc dlaczego znowu je kupujemy? Niestety mało kto uważa, że coś co dopuszczono do sprzedaży może zagrażać zdrowiu.
 Poznaj prawie 100-letnią historię kuchenki mikrofalowej i szokujące fakty na temat posiłków w niej podgrzewanych.
 Dowiedz się jak takie jedzenie działa na organizm ludzki.
 Jedzenie z mikrofalówki – powolna śmierć na własne życzenie.
 Początki – mikrofale w służbie dla III Rzeszy.
 „Hans, ale zimno dzisiaj w tym ruskim lesie… Chodź, ogrzejemy się przy radarze.” Tak to było na początku.
 Niemcy używali początkowo mikrofal do systemów radarowych.
 Jednak problemem dla owych żołnierzy było rozwijanie się raka krwi.
 Radary oparte na działaniu mikrofal dały początek nowemu ustrojstwu.
 Jeśli można było podgrzewać żywą tkankę żołnierzy, to można było też podgrzewać dla nich jedzenie na wielu posterunkach podczas walk z Rosjanami.
 Praktyczne może to i było, ale owi żołnierze także chorowali na raka krwi (białaczkę), od samego spożywania posiłków odgrzewanych w kuchenkach mikrofalowych.
 Ich odporność drastycznie spadała.
 Zakazano używania tego urządzenia w całej III Rzeszy.
 Mimo, że od tamtych wydarzeń minęło około 80 lat, kuchenki tego typu nie zmieniły zasady działania.
 Może mają ładniejszy dizajn i są bardziej kompaktowe, ale mikrofale to nadal mikrofale.
 Kuchenka mikrofalowa generuje krótkie fale, które w czasie jednej sekundy przeciągają cząsteczki tam i z powrotem miliardy razy.
 Ten ruch generuje dużo ciepła i tak potrawa staje się gorąca.
 Ale to nie wszystko.
 Kawałki jedzenia w czasie tego procesu są targane, szarpane i rozrywane na bardzo drobne i chaotyczne frakcje, które zmieniają swój pierwotny skład chemiczny i przybierają zupełnie inne formy połączeń cząsteczkowych.
 Opinia pewnego fizyka : „Działanie kuchenki mikrofalowej wygląda tak.
 Jedzenie jest bombardowane strumieniem cząstek, w wyniku czego atomy przechodzą do stanu wzbudzonego, a zatem zwiększa się jego energia wewnętrzna.
 Następnie po wyjęciu pożywienia z kuchenki następuje szybki proces stygnięcia, w wyniku fizycznego dążenia materii do osiągnięcia minimum stanu energetycznego.
 Elektrony wzbudzonych atomów powracają na właściwe powłoki energetyczne, dokonując przeskoków o jedną lub dwie powłoki.
 W tym momencie emitowane są kwanty promieniowania Gamma, które jest najbardziej przenikliwe.
 Spożywając taki pokarm narażamy się od wewnątrz na promieniowanie Gamma i jonizację atomów własnego organizmu.
 To prowadzi do  nieodwracalnych procesów niszczenia komórek, a także do radiolizy wody w organizmie.
 Skutki tego procesu prowadzą do zamierania oraz mutacji komórek, co w efekcie prowadzi do poważnych zmian chorobowych, w tym nowotworów.”   Jak organizm widzi takie jedzenie? Problem w tym, że go nie widzi jako coś co można uznać za odżywcze.
 Wręcz przeciwnie.
 Nie dość, że nie możesz wykorzystać takiego posiłku na zdrowie, w tej sytuacji staje się on toksycznym radioaktywnym balastem.
 Bardzo często zdarza się, że organizm nie wie co począć z takim fantem.
 Felerne i niczemu niepodobne cząsteczki mogą utknąć w głębokich partiach tkanek, albo będą tam celowo upychane (obrzęk limfatyczny).
 Tak naprawdę, oprócz „zapchania kiszki” nie zyskujesz nic na posiłku poddanym działaniu mikrofalówki.
 Kiedy widzę jak rodzic podgrzewa mleko , albo jedzonko dla dziecka w mikrofali, mój mózg gotuje się… Nawet chwilowe podgrzewanie jedzenia w kuchence mikrofalowej wystarczy, żeby nie mieć pożytku z witamin i minerałów w nim zawartych.
 Witaminy grupy B są odpowiedzialne za prawidłowa pracę układu nerwowego i odporność na stres.
 C i E – witaminy życia i silne przeciwutleniacze, również odpowiedzialne za zwalczanie chorób serca i przeciwdziałające nowotworom.
 Wszystko na nic, jeśli uruchomisz tę buczącą skrzynkę! Forensic Research Document of Agricultural and Resource Economics przeprowadziło badania pod kątem działania kuchenek mikrofalowych na jedzenie.
 William P. Kopp stwierdza: „Skutki spożywania potraw gotowanych w kuchenkach mikrofalowych są długofalowe i trwałe w ludzkim organizmie.
 Wartość mineralna i witaminowa jest na tyle ograniczona, że organizm nie ma praktycznie żadnych korzyści z takiego posiłku.
 Jednocześnie musi przyjąć cząsteczki, których nie jest w stanie rozłożyć.”     Promieniowanie kuchenek.
 Mówi się, że obecne kuchenki są świetnie zabezpieczone przed uwalnianiem na zewnątrz promieniowania.
 Jednak stanie w pobliżu kuchenki w czasie jej pracy zwiększa prawdopodobieństwo oddziaływania fal na mózg.
 Mózg jest bardzo czuły na tego rodzaju energię.
 Przypomina mi się scena z filmu o hakerach, którzy resetują dyski i płyty z zapisem, wrzucając je do kuchenki, w czasie pukania do drzwi służb specjalnych.
 😉 Twój mózg tez jest bazą danych i do tego pracującą na nich.
 Nie daj się ogłupić, że mikrofale są obojętne dla zdrowia.
 Wiele osób potwierdza, że rzekoma klatka Faradaya wcale nie jest taka szczelna, a wskaźniki pokazują przekroczone normy w odległości do 1m od kuchenki.
 Jedzenie potraw z mikrofalówki jest przyczyną wielu chorób.
 Przeanalizuj je i zastanów się czy nie pokrywają się z twoimi chorobami i zwyczajem jedzenia z mikrofalówki: problemy układu trawiennego, depresja, zaburzenia koncentracji, bałagan myślowy, problemy ze snem, utrata pamięci, uszkodzenia mózgu, wyczerpanie adrenalinowe, zawroty głowy, wysokie ciśnienie krwi, nerwowość, zaćma, zapalenie wyrostka robaczkowego, migreny, bóle żołądka, wypadanie włosów, choroba serca, zaburzenia układu limfatycznego, nowotwory żołądka i jelit.
 Jedzenie z mikrofalówki wprowadza nasz organizm w silny stres.
 Szwajcarskie naukowiec Hans U. Hertel podkreśla, że „wysoki poziom cholesterolu we krwi nie jest tak zależny od jego zawartości w pożywieniu, jak od poziomu stresu”.
 Nawet najzdrowsze warzywa poddane obróbce w mikrofali podnoszą znacząco poziom cholesterolu.
 Złe jedzenie stresuje organizm.
 Głównym źródłem cholesterolu jest wątroba, która w ciągu godziny może wyprodukować kilkaset razy więcej cholesterolu niż tego zawartego w jednym kotlecie czy jajku.
 W 90% domów Amerykanów, kuchenka mikrofalowa jest urządzeniem codziennego użytku.
 Polska wcale nie jest daleko od tej ‚normy’.
 To co niegdyś na podstawie obserwacji było uznane za rakotwórcze, teraz wróciło do naszych domów pod przykrywką ”ekonomicznego urządzenia”.
 Podobnie jak w wypadku produktów spożywczych wątpliwej jakości, wystrzegaj się dalekich od natury metod przyrządzania jedzenia.
 Żaden koncern produkujący śmieciowe sprzęty AGD, nie zwraca uwagi na kwestie natury moralnej, dopóki światem rządzą ludzie związani finansowo z przemysłem.
 Jeśli posiadasz mikrofalówkę w domu, lepiej pozbądź się tej puszki pandory.
 Mnie od zawsze jedzenie z mikrofali jakoś dziwnie śmierdziało, dlatego nigdy nie posiadałem tego ‚precjoza’.
 Oby domy wyposażone w tą śmiercionośną technologię wróciły do normalnych metod gotowania i podgrzewania posiłków.
 Tego wszystkim życzę.
 Na tym blogu znajdziesz więcej artykułów o kuchence mikrofalowej, które otwierają oczy.
 Udostępnij te informacje znajomym, szczególnie tym, którzy posiadają mikrofalówkę.

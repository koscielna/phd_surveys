 Po terminie porodu Niektórym kobietom końcówka ciąży bardzo się dłuży – wyprawka gotowa, walizka do szpitala spakowana, a sama ciężarna jest w pełni gotowa na poród i nowy rozdział w życiu.
 Innym ostatni trymestr ciąży niemal umyka w natłoku spraw do załatwienia i planów dotyczących nowego życia po porodzie.
 W obu przypadkach nie sposób jednak przegapić tej specyficznej daty – terminu porodu.
 Bardzo często zdarza się jednak, że mija on jak zwykły dzień, a maleństwu nie spieszy się z opuszczeniem wygodnego brzucha mamy.
 Co wówczas robić? Zanim w miejsce radosnego oczekiwania i podniecenia wkradnie się niepokój, warto przypomnieć sobie podstawowe informacje na temat czasu trwania ciąży.
 Kalendarz ciąży liczy 40 tygodni, przy czym w jego skład wchodzi cały cykl owulacyjny, w którym dochodzi do zapłodnienia.
 Do połączenia komórki jajowej z plemnikiem dochodzi dopiero w trzecim tygodniu tak liczonej ciąży.
 Tym sposobem rodzi się problem z obliczeniem daty porodu.
 Niektórzy lekarze określają ją na podstawie wieku ciąży (od poczęcia dziecka), inni – w zgodzie z medycznymi uwarunkowaniami – od ostatniej przed roczną przerwą menstruacji.
 Dlatego termin porodu jest zwykle tylko hipotetyczną datą rozwiązania.
 Co ciekawe, niewiele dzieci rodzi się dokładnie w terminie (nie wliczając porodów drogą cesarskiego cięcia).
 Do wyliczonej daty porodu należy więc często dodać (lub też odjąć) około dwa tygodnie.
 Jeżeli jednak minął 42 tydzień ciąży, a dziecko nadal nie zdradza chęci do wyprowadzki z łona matki, mamy do czynienia z ciążą przenoszoną.
 Należy podjąć odpowiednie kroki, mające na celu pomóc dziecku w przyjściu na świat.
 Pierwszym krokiem jest konsultacja z lekarzem prowadzącym ciążę.
 Wizyta nie jest niczym niezwykłym – pod koniec ciąży kobieta bywa u ginekologa częściej niż w I czy II trymestrze.
 Co może doradzić lekarz? Po zbadaniu przyszłej mamy i pozytywnej ocenie stanu łożyska i jakości płynu owodniowego prawdopodobnie poleci naturalne sposoby przyspieszenia porodu i zaprosi kobietę na regularne badanie KTG.
 2.
 Domowe sposoby na wywołanie porodu Wywołanie porodu naturalnymi środkami nie ma na celu przyspieszenia akcji porodowej do tego stopnia, że poród rozpocznie się w domu.
 Metody te mają raczej wspomóc produkcję oksytocyny odpowiedzialnej za skurcze porodowe oraz zmiękczyć i przygotować szyjkę macicy do pełnego rozwarcia.
 Najpopularniejszym sposobem na wywołanie porodu jest… seks.
 Stymulowanie pochwy, szczytowanie oraz drażnienie sutków powoduje wydzielanie się oksytocyny i skurcze macicy – stosunek płciowy więc nie tylko przyspiesza skurcze porodowe, ale także ćwiczy mięśnie macicy, które będą bardzo intensywnie pracować podczas rodzenia.
 Innymi domowymi metodami przyspieszania porodu jest picie herbaty z liści malin, spożycie na czczo dwóch łyżek oleju rycynowego (jest on znany z właściwości przeczyszczających; w tym przypadku wspomoże on pracę jelit, a tym samym da macicy miejsce do kurczenia się i wyciągania) czy łykanie wiesiołka.
 Pomocne są także spacery i intensywniejszy wysiłek fizyczny (np. chodzenie po schodach, szybki marsz).
 Naturalne wywoływanie porodu ma istotną przewagę nad sztuczną indukcją porodu – nie wiąże się z nim jakiekolwiek ryzyko dla matki i dziecka.
 3.
 Poród indukowany Poród wywoływany to taki, którego rozpoczęcie jest przyspieszone sztucznym wywołaniem skurczy porodowych.
 U większości ciężarnych rozwiązanie następuje w naturalny sposób, gdy zbliża się termin porodu, pomiędzy 37. a 42. tygodniem ciąży.
 Niekiedy jednak wywoływanie porodu jest konieczne.
 Istnieje szereg wskazań do zastosowania metod indukcji porodu, ale w Polsce procedury te są nadużywane.
 Mimo iż Światowa Organizacja Zdrowia zaleca, by wywoływanie porodu po terminie nie dotyczyło więcej niż 10% wszystkich porodów, w naszym kraju ten odsetek przekracza aż 50%.
 Kiedy wywoływanie porodu jest rzeczywiście niezbędne? 3.1.
 Wskazania do wywołania porodu Sztuczne wywołanie skurczy porodowych jest stosowane wtedy, gdy:     kobieta choruje na cukrzycę ciążową;     ciężarna ma wysokie ciśnienie krwi;     istnieje zagrożenie zatruciem ciążowym;     minął termin porodu, a ciąża trwa dłużej niż 41 tygodni – ryzyko zagrożenia zdrowia dziecka jest wówczas znacznie wyższe, ponieważ maluch staje się coraz większy i ma mniej miejsca w łonie matki, ponadto może dojść do utraty przez dziecko możliwości swobodnego oddychania i do zachłyśnięcia się smółką;     mimo pęknięcia pęcherza płodowego nie pojawiły się samoistne skurcze.
 Jeśli twój lekarza rozważa wywołanie porodu, powinien dokładnie poinformować cię nie tylko o szczegółach indukcji porodu, ale i o alternatywach dla tej procedury.
 Masz prawo poznać plusy i minusy wywoływania porodu.
 Twoja zgoda jest niezbędna do podjęcia przez lekarzy wszelkich działań.
 3.2.
 Metody wywoływania porodu Przyspieszenie rozpoczęcia akcji porodowej dokonuje się na kilka sposobów.
 Najbardziej powszechne to:     podanie za pomocą kroplówki oksytocyny – jest to syntetyczny hormon, który wywołuje skurcze;     podanie żelu prostaglandynowego – dopochwowo,odklejenie dolnego bieguna pęcherza płodowego – jest to stosunkowo najmniej inwazyjna metoda, którą stosuje się u kobiet będących w ciąży dłużej niż 41 tygodni.
 3.3.
 Skutki uboczne wywoływania porodu Decydując się na sztuczną indukcję porodu, należy być świadomym potencjalnych zagrożeń.
 Są to:     większe ryzyko niedotlenienia dziecka;     bardziej bolesne i silniejsze skurcze niż podczas naturalnego porodu;     wyższe ryzyko krwotoku poporodowego;     uszkodzenia szyjki lub macicy, a także przedwczesnego oddzielenia się łożyska;     większe prawdopodobieństwo cesarskiego cięcia;     wzrost ryzyka pojawienia się u dziecka żółtaczki poporodowej (po zastosowaniu oksytocyny lub prostaglandyn).
 Wywoływanie porodu nie zawsze jest koniecznością.
 Jeśli minął termin porodu twojego dziecka, wypróbuj naturalne sposoby na indukcję porodu.
 Jeżeli nie będą skuteczne, porozmawiaj z lekarzem i dowiedz się, czy jego zdaniem sztuczne przyspieszanie rozpoczęcia porodu jest dobrym pomysłem.

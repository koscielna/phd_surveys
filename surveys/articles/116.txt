 Szczepienie przeciwko grypie obniża także ryzyko wystąpienia chorób w ogóle z infekcjami niekojarzonych.
 Mamy na to mocne dane.
 Właśnie w ten weekend przypada arytmetyczna połowa optymalnego okienka czasowego dla szczepienia przeciwko grypie.
 Najwyższa więc pora, by przypomnieć trochę faktów, niekoniecznie powszechnie znanych.
 Zastrzegam na wstępie, że poniższe przypomnienie nie jest przeznaczone dla wszystkich.
 Zdeklarowani przeciwnicy szczepień powinni oszczędzić sobie czasu i energii na czytanie.
 Oni i tak wiedzą lepiej – co jest równoznaczne z decyzją poddania się procesowi klasycznej, darwinowskiej selekcji.
 Jeżeli będą wystarczająco silni, a szczęście będzie im wystarczająco sprzyjać, to przeżyją bez żadnego szwanku.
 Jeżeli zaś nie przetrwają, to mogą pocieszać się, że przynajmniej wrócili do korzeni, bo przez przeważający czas trwania ludzkości to właśnie infekcje były jednym z trzech głównych czynników ograniczających liczebność jej populacji.
 Zdeklarowani zwolennicy szczepień również nie są adresatami tego tekstu.
 Z pewnością już się zaszczepili albo szykują się do szczepienia, więc również ich czasu szkoda – chociaż oczywiście z zupełnie innych powodów.
 No, chyba że szukają argumentów, by kogoś do szczepień przekonać.
 Poniższe dane powinni natomiast poznać ci, którzy się w kwestii szczepienia przeciwko grypie wahają.
 Mam świadomość, że są świadkami sporu toczącego się w tej materii – i to sporu niesymetrycznego.
 Bo ze szczepieniami jest tak jak z wieloma innymi przedmiotami sporów, toczących się dzisiaj nie tylko w Polsce: tym, którzy adresują swoje wywody do rozumu, zawsze trudniej dotrzeć do ogółu niż tym, którzy odwołują się do intuicji i emocji.
 Dlatego, że przekaz tych drugich jest zazwyczaj bardziej dosadny.
 Nic dziwnego: człowiek jest stworzeniem najpierw emocjonalnym, a dopiero potem intelektualnym.
 Efekt końcowy jest taki, że największa grupa społeczeństwa to ci z wątpliwościami.
 Może by się i zaszczepili, ale wątpliwości narastają.
 Z jednej strony mądre w treści, ale nieefektowne w formie wypowiedzi fachowców.
 Z drugiej – krzyk o szczepionkowym ludobójstwie i straszenie spiskiem firm farmaceutycznych, wspierane przez niektóre media (do tego jeszcze wrócimy).
 Pozwalam sobie dosypać trochę danych do szalki racjonalnych uzasadnień, mając nadzieję, że przynajmniej u niektórych z Państwa przeważy ona szalę obaw na stronę decyzji o szczepieniu przeciwko grypie.
 Zostawmy dzisiaj z boku przepychankę dotyczącą ogólnej skuteczności szczepienia „zachorują – nie zachorują”, a także klechdy o masowych, ciężkich efektach niepożądanych.
 Przyjrzyjmy się wpływowi szczepień przeciwko grypie na występowanie i przebieg chorób serca i układu krążenia – w formie prostych wniosków z opublikowanych prac i dokumentów.
 Oczywiście ograniczę się do najważniejszych przykładów.
 Grypa może być czynnikiem wyzwalającym tzw. atak serca – czyli ostry epizod wieńcowy: zawał serca lub niedokrwienie serca mogące prowadzić do zawału – takie stanowisko opublikowało w 2006 r., na podstawie istniejących już wówczas danych, American Heart Association.
 Zjawisko być może wynika stąd, że sama miażdżyca jest procesem zapalnym i grypa może zaostrzać proces miażdżycowy (przepraszam fachowców za duże uproszczenie), powodując zamykanie naczyń tętniczych.
 Dokładnego mechanizmu nie znamy.
 Sceptykom spieszę przypomnieć, że niewątpliwie istniejące w przyrodzie zjawiska, których mechanizmu nie zdołano poznać, nie są zupełną rzadkością.
 Najlepszym przykładem może być grawitacja.
 Nie tylko grypa niesie za sobą ryzyko niedokrwienia serca.
 Opublikowano dane wskazujące na to, że również inne infekcje mogą prowokować wystąpienie ostrego epizodu wieńcowego.
 Szczepienie przeciwko grypie zmniejsza ryzyko względne groźnych epizodów kardiologicznych o 36 proc. (Udell, Ciszewski i wsp., JAMA, 2013).
 Definicja groźnego epizodu kardiologicznego (MACE – Major Adverse Cardiac Event) obejmuje łącznie następujące wydarzenia: zgony sercowe, zawały serca oraz konieczność wykonania plastyki tętnic wieńcowych w celu zlikwidowania ostrego niedokrwienia.
 Publikacja jest metaanalizą badań randomizowanych, w których grupy kontrolne nie były szczepione.
 Łącznie objęto nimi 6735 pacjentów.
 W badaniu tym nie wykazano istotnego statystycznie spadku samej śmiertelności w grupie szczepionej.
 Musimy jednak zdawać sobie sprawę, że zawał serca może spowodować istotne inwalidztwo poprzez znaczne zmniejszenie sprawności pracy serca.
 Szczepienie przeciwko grypie zmniejsza względne ryzyko zgonu sercowego u pacjentów z chorobą wieńcową o 55 proc.
 (!) (Clar i wsp., Cochrane Database 2015) – jest to metaanaliza obejmująca 12029 pacjentów.
 Tutaj komentarzy nie trzeba.
 W pojedynczych, mniejszych badaniach ryzyko MACE u pacjentów z chorobą wieńcową było istotnie statystycznie niższe.
 Jakie są zatem wskazania do stosowania szczepień przeciwko grypie u chorych kardiologicznych? Zróbmy króciutki przegląd wytycznych opublikowanych przez niezależne towarzystwa lekarskie.
 Zaleca się szczepienie przeciwko grypie wszystkich pacjentów z przewlekłą chorobą układu krążenia – napisano w wytycznych wspólnie wydanych przez w 2011 r. przez dwa główne towarzystwa skupiające kardiologów w USA: American Heart Association i American College of Cardiology.
 Jest to z wskazanie bardzo twarde.
 Podobnie jak dwa kolejne.
 Zaleca się szczepienie przeciwko grypie wszystkich pacjentów z przewlekłą chorobą układu krążenia (US Center for Disease Control and Prevention, 2013).
 Coroczne szczepienie przeciwko grypie powinno być oferowane wszystkim pacjentom bez przeciwwskazań – wytyczne American Heart Association (2015) dla chorych po założeniu pomostów aortalno-wieńcowych (czyli tzw. by-passów).
 Jest to wskazanie klasy I, czyli najwyższej możliwej.
 U pacjentów z nadciśnieniem płucnym zaleca się szczepienie przeciwko grypie – jako prewencję zapalenia płuc, w którym śmiertelność dla tej grupy wynosi aż 7 proc. (Wytyczne ESC 2015).
 Również to wskazanie jest jednoznaczne.
 Można rozważyć coroczne szczepienie przeciwko grypie u osób z chorobą układu krążenia – czytamy w wytycznych Europejskiego Towarzystwa Kardiologicznego (ESC) z 2016 r., dotyczących ogólnej profilaktyki chorób układu krążenia.
 Jest to wskazanie miękkie.
 Najprawdopodobniej autorzy wytycznych uznali, że w świetle istniejących danych nie można powiedzieć, że szczepienie przeciwko grypie chroni przed rozwojem i powikłaniami wszystkich chorób układu krążenia.
 Pozytywne dane mamy jedynie w odniesieniu do choroby wieńcowej i jej najgroźniejszych powikłań: zawału serca i zgonu sercowego.
 Różnica podejścia pomiędzy wytycznymi w USA i Europie może (chociaż nie musi) w tym przypadku wynikać z różnic implikacji prawnych nieoptymalnego postępowania lekarza, jakie istnieją pomiędzy obydwoma obszarami.
 W USA strach przed roszczeniami pacjenta i jego prawnikiem jest nadal znacznie wyższy.
 Tzw. wyszczepialność w Polsce wynosi (w zależności od źródła) 3-5 proc. populacji, podczas gdy w krajach „starej” Unii Europejskiej nierzadko znacznie przekracza 50 proc. Tam jednak prowadzono intensywne kampanie społeczne i medialne.
 U nas rola mediów bywa niejednoznaczna.
 Rekord pobił ostatnio popularny portal medonet.
 Pani Halina Pilonis zaprosiła do rozmowy o szczepieniach najpierw lekarza z Warszawskiego Uniwersytetu Medycznego, dr.
 Tadeusza Zielonkę.
 Polecam ten wywiad, bo jest w nim sporo ważnych i interesujących danych.
 Następnie pani Pilonis przeprowadziła rozmowę z Justyną Sochą z ruchu antyszczepionkowego.
 Poglądy tej pani są jej prywatna sprawą, tak samo jak zerowy poziom merytoryczny, maskowany efektownymi tyradami.
 Nie ma w tym nic nagannego – Justyna Socha jest bowiem agentem ubezpieczeniowym.
 Można tę informację uzyskać bez trudu, posługując się popularną wyszukiwarką.
 Tak się składa, że znam agentów ubezpieczeniowych.
 Jestem pełen głębokiego szacunku dla ich wiedzy w zakresie określania ryzyka różnych nieprzyjemnych zdarzeń oraz zmniejszania tego ryzyka.
 Jednak żaden z tych – naprawdę znakomitych – fachowców nigdy nie wypowiadał się autorytatywnie na tematy medyczne, ja zaś pokornie nie próbuję nawet dyskutować z nimi na tematy związane z ubezpieczeniami.
 Portal medonet i Halina Pilonis zapomnieli napisać, że Justyna Socha nie ma zawodowo nic wspólnego z medycyną ani nawet naukami biologicznymi.
 Dopuścili się karygodnej manipulacji w imię napędzania oglądalności.
 Skoro zaś renomowany portal podlega takiej tabloidyzacji, to łatwo sobie Państwo mogą wyobrazić, co znajduje się na stronach z definicji nieprofesjonalnych.
 Dlatego apeluję – w swoich decyzjach dotyczących spraw medycznych polegajcie Państwo na fachowcach.
 Aha, i jeszcze jedna kwestia: powtarzane jak mantra zdanie, że szczepienia to spisek firm farmaceutycznych.
 Warto poczytać trochę historii (ma być jej więcej w szkołach, więc może na ten wątek też znajdzie się czas) i dowiedzieć się, że jeszcze nie tak dawno temu głównymi przyczynami zgonów w Europie były infekcje, urazy oraz niedożywienie.
 Z infekcjami poradzono sobie dzięki szczepionkom i antybiotykom.
 Jedne i drugie produkowane są przez wyspecjalizowane firmy, bo krasnoludki jakoś zanikły albo się rozbestwiły (nie mam dokładnych danych).
 Analogicznie można by stwierdzić, że ciepłe, nieprzemakalne ubrania to spisek firm odzieżowych, buty – obuwniczych, okulary – optycznych… i tak dalej, aż do pełnego obłędu.
 Optymalny termin szczepienia określany jest na wrzesień-październik lub wrzesień-listopad.
 Jest jeszcze zatem sporo czasu, by się w nim zmieścić.
 Późniejsze szczepienie też ma sens, ale nie warto zwlekać, bo szczyt zachorowań na grypę, który przypada na okres pomiędzy styczniem a marcem, zbliża się nieuchronnie.

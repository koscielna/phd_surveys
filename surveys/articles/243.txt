 Zerwanie wędzidełka Wędzidełko to elastyczna tkanka pod żołędzią penisa, która łączy go z napletkiem i pomaga napletkowi w kurczeniu się.
 Zazwyczaj napletek oddziela się od żołędzi w okresie niemowlęcym, ale u niektórych mężczyzn proces ten trwa dłużej.
 Gdy dojdzie do oddzielenia się napletka od żołędzi, napletek stopniowo się rozciąga, by mógł cofnąć się za żołądź nawet podczas erekcji.
 Jeśli jednak napletek jest bardzo ciasny i niezdolny do cofnięcia, może powodować dyskomfort w czasie stosunku płciowego.
 W czasie intensywnych pieszczot przy niewielkiej lubrykacji może dojść do zerwania wędzidełka.
 Oprócz bólu pojawia się wówczas krwawienie.
 Postępowanie przy zerwaniu wędzidełka W większości przypadków wystarczy wstrzymać się od stosunków płciowych oraz masturbacji, aż wędzidełko się zagoi.
 Rana powinna być utrzymywana w czystości, nie zaleca się przy tym stosowania jakichkolwiek maści przyspieszających gojenie.
 W miejscu pęknięcia wędzidełka może pojawić się niewielka blizna.
 W czasie kąpieli lub prysznica zaleca się delikatne odsuwanie napletka.
 Systematyczne wykonywanie tej czynności pozwala na stopniowe rozciągnięcie napletka.
 Jeśli jednak mimo starań nie udaje się uzyskać właściwego stanu napletka, istnieje realne ryzyko ponownego zerwania wędzidełka.
 W takiej sytuacji należy rozważyć poddanie się obrzezaniu.
 Właściwe cofanie się napletka może być utrudnione poprzez zbyt wąskie lub krótkie wędzidełko, a częste urazy wędzidełka mogą przyczynić się do powstawania blizn.
 Tkanka, która uległa zbliznowaceniu nie rozciąga się, dlatego po pierwszym zerwaniu wędzidełka mogą pojawić się kolejne urazy.
 Operacyjne leczenie wędzidełka Najlepszą metodą leczenia ciasnego wędzidełka lub jego pęknięć jest operacja wydłużenia wędzidełka.
 Zabieg polega na przecięciu tkanki i umieszczeniu rozpuszczalnych szwów blisko krawędzi skóry w przeciwnym kierunku do linii cięcia.
 W ten sposób uzyskuje się przedłużenie wędzidełka.
 Operacja jest mało skomplikowana i można ją przeprowadzić w znieczuleniu miejscowym lub ogólnym.
 Nie można jednak zagwarantować, że obejdzie się bez ryzyka, a wynik zabiegu będzie zadowalający.
 Operacja wydłużenia wędzidełka przynosi zwykle pożądane efekty, ale czasami blizny znów twardnieją.
 W takiej sytuacji jedynym wyjściem może być całkowite obrzezanie.
 Penis jest mocno ukrwiony, dlatego po operacji wędzidełka może pojawić się krwawienie.
 Ubytek krwi nie stanowi jednak zazwyczaj wystarczającego powodu, by pacjent musiał zostać w szpitalu.
 Przez trzy pierwsze dni po zabiegu wskazane jest leżenie.
 W takiej pozycji penis jest bowiem wyżej niż reszta ciała.
 Odradza się z kolei siedzenie.
 W przerwach między leżeniem pacjent powinien co jakiś czas przejść się, na przykład idąc do toalety lub na posiłek.
 Okolice narządów płciowych powinny być utrzymywane w suchości przez pierwsze 24 godziny od operacji.
 Dopiero później można brać prysznic.
 Po 5-6 dniach można dwa razy dziennie brać gorącą kąpiel z solą, by złagodzić obrzęk i dyskomfort.
 Przez 2-3 tygodnie po zabiegu nie należy intensywnie ćwiczyć i uprawiać seksu.
 Po tym czasie wskazane jest nakładanie prezerwatyw podczas stosunku, by ochronić świeżo wygojoną skórę.

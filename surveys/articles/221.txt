 W organizmie człowieka codziennie dochodzi do powstawania komórek z uszkodzonymi DNA, tak… potencjalnie są to komórki nowotworowe.
 Dlaczego więc wszyscy ludzie nie mają nowotworów ? Dzieje się tak głównie dlatego, że nasz układ odpornościowy (immunologiczny) potrafi takie komórki zidentyfikować i unicestwić.
 Kiedy jednak układ odpornościowy danego człowieka nie pracuje prawidłowo, wtedy nie jest w stanie wykonać pracy do jakiej został stworzony, czyli nie potrafi zidentyfikować komórek nowotworowych i ich zniszczyć.
 Efektem tej dysfunkcji układu odpornościowego, co najczęściej jest widoczne (ale często bardzo długo nie jest widoczne) jako „guz nowotworowy”.
 Guz nowotworowy, który przecież nie powstał bez przyczyny jest symptomem wskazującym na to, że u takiej osoby system odpornościowy nie funkcjonuje tak jak trzeba.
 Dlatego można powiedzieć, że  nie istnieje „choroba nowotworowa”, lecz istnieje właśnie choroba układu odpornościowego.
 Dlaczego więc medycyna akademicka z taką nieokiełznaną upartością walczy z symptomem, a nie z przyczyną ? Medycyna ta za wszelką cenę chce zniszczyć symptom, czyli guz nowotworowy.
 Ma tylko na to trzy sposoby:     zatruć chemioterapią, przy okazji potwornie zatruwając całego pacjenta,     „spalić” poprzez naświetlanie promieniami, które tak jak chemioterapia sprzyjają powstaniu nowotworów.
 wyciąć guz, nigdy nie mając pewności, że wycięto wszystko co złe.
 Ale co z przyczyną ?… Co z leczeniem układu odpornościowego ? Tego się nie leczy ! A jeśli na przykład  u kogoś jest podwyższony poziom cholesterolu, to musi być też jakaś przyczyna tego, która powoduje, że organizm (głównie wątroba) zaczyna przyspieszać jego produkcję.
 Jaki jest powód tego ? Najczęściej wysoki poziom cholesterolu we krwi jest wskaźnikiem (markerem), że coś złego się dzieje.
 Czyli wysoki poziom cholesterolu jest symptomem stanu chorobowego  – najczęściej zapalnego.
 Dlaczego więc, z maniakalną upartością, zupełnie nie zwracając uwagi na możliwą przyczynę, obniżamy poziom tego markera statynami, czyli też toksynami ? Cholesterol jest transporterem białek niezbędnych do budowy i regeneracji tkanek.
 Obniżając jego ilość zmniejszamy dowóz niezbędnych elementów budulcowych tkanek.
 Jaki to ma wtedy sens ? Żaden, a jednak statyny obniżając poziom cholesterolu są przepisywane w ilościach przerażających.
 Bezmyślnie obniża się poziom markera nie zwracając zupełnie uwagi na przyczynę, jaka  powoduje jego podwyższony poziom.
 W przypadku zaś łuszczycy, gdzie zmiany na skórze symptom tej choroby, leczy się sterydami, maściami, balsamami, a nie przyczyny tkwiące w chorobie układu odpornościowego.
 W przypadku chorób przewlekłych jesteśmy tam, gdzie byliśmy dziesiątki lat temu.
 W tych schorzeniach „leczy się” (stara się usuwać) symptomy lub raczej łagodzi się ich skutki.
 Nie ma odpowiedzi na to, jak wyleczyć np. stwardnienie rozsiane, cukrzycę, nadciśnienie, toczeń, mukowiscydozę, sklerodermię, przewlekłe zapalenie tarczycy (choroba Hasimoto), wrzodziejące zapalenie jelita grubego, astmę, osteoporozę czy nowotwory itd.
 Lecząc, powoduje się poprzez stosowanie aspiryny, ibupromu, ketonalu, acardu, powiększanie problemów, gdyż „leki” te obniżają skuteczność działania układu odpornościowego.
 Wiele z nich kupić można po prostu bez recepty, np. w sklepie przy kasie.
 Ocenia się, że tylko w USA ciężkie skutki uboczne zastosowanych leków występują u ponad 2,2 miliona ludzi rocznie, a ponad 100.000 osób z tego powodu umiera rocznie.
 Nie można zapomnieć o takiej prostej rzeczy:     „Organizm człowieka nie cierpi na żadną chorobę z powodu braku leku, ale z powodu braku substancji naturalnych, których to brak doprowadził do choroby” –  J.Zięba „UKRYTE TERAPIE” s. 42.
 Do prawidłowego funkcjonowania organizmu nie potrzeba aspiryny, heparyny, ibuprofemu, statyn i tysięcy innych substancji produkowanych sztucznie.
 Natomiast koniecznie potrzeba nam: witamin, minerałów, pierwiastków śladowych, aminokwasów, tłuszczy i białek, czystego powietrza, czystej wody i przede wszystkim naturalnej żywności, nieprzetworzonej, niemodyfikowanej, nie rektyfikowanej.
 Czy firmom farmaceutycznym zależy na tym, żeby wyprodukować lek, który miliony ludzi w krótkim czasie wyleczy? To przecież oznaczałoby dla  nich katastrofę.
 To się nie opłaca.
 Firmom tym nie zależy, żeby wyleczyć, tylko, żeby leczyć bez końca.
 I tak się dzieje.
 Kupujemy leki bez końca.
 Leki, które przyniosą ulgę, ale …nie wyleczą.
 To takie proste… „Wielką tragedią ludzkości jest to, że dawno temu, farmacja służyła lekarzowi, a w dzisiejszych czasach lekarz jest sługą farmacji! – J. Zięba „UKRYTE TRERAPIE”  s. 49.

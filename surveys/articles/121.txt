 Rozpoznanie nowotworu nie jest łatwą sprawą.
 Potwierdzenie tej poważnej choroby można uzyskać tylko na podstawie profesjonalnych badań, jednak pierwsze objawy pojawiają się zazwyczaj dużo wcześniej.
 Niestety – często je bagatelizujemy.
 Nie bez przyczyny zresztą, bywają one niecharakterystyczne i można je pomylić z innymi schorzeniami.
 Wyjściem z sytuacji jest obserwowanie własnego ciała.
 Jeśli tylko zauważysz coś, co cię niepokoi, a na dodatek zmiana będzie postępowała – udaj się do lekarza.
 Nie musi być to oczywiście symptom nowotworu, ale lepiej to wykluczyć niż przeczekać.
 Wzdęcia To coś, co towarzyszy sporej części kobiet, zwłaszcza przed miesiączką.
 Wzdęcia bywają bolesne i męczące.
 Jeśli mijają szybko – mogą być skutkiem zbyt szybko zjedzonej potrawy lub kłopotów trawiennych.
 Pomoże na nie napar z rumianku i kopru włoskiego.
 Jeśli natomiast wzdęcia pojawiają się na początku cyklu menstruacyjnego, a do tego dochodzą bolesne zaparcia oraz uczucie pełności – może to sygnalizować nowotwór jajnika bądź macicy.
 Symptomy tych chorób bardzo długo nie są typowe, dlatego kobiety tak często je bagatelizują, zwalając winę na niedopowiednią dietę.
 Warto zatem już wcześniej udać się do ginekologa.
 Ostry ból żołądka może świadczyć o wrzodach lub zakażeniu bakterią Helicobacter pylori, ale może być też sygnałem, że w twoim organizmie dzieje się coś dużo bardziej niepokojącego.
 O ile bowiem wrzody żołądka czy zakażenie bakteryjne można dość szybko wyleczyć, o tyle nowotwór w zaawansowanym stadium jest chorobą śmiertelną.
 Jeśli więc cierpisz na uporczywe, silne i długotrwałe bóle żołądka przypominające skurcz, regularnie miewasz mdłości, towarzyszy ci brak apetytu przy jednoczesnym uczuciu pełności – to może być sygnał, że masz początkowe stadium nowotworu żołądka.
 Takie objawy mogą również świadczyć o raku wątroby, raku trzustki bądź raku jelita grubego.
 Optymalną dla człowieka temperaturą ciała jest 36,6 stopni Celsjusza.
 Wtedy wszystkie układy naszego organizmu pracują prawidłowo.
 Jeśli jednak temperatura ulega podwyższeniu, jest to pierwszą odpowiedzią immunologiczną na atakujący organizm patogen.
 Jeśli masz podwyższoną temperaturę bądź gorączkę, to znak, że organizm walczy z intruzem.
 Jeśli oprócz tego często się przeziębiasz – wykonaj morfologię krwi.
 To podstawowe badanie pokaże, czy w twoim organizmie wszystko jest w porządku.
 Dlaczego warto je wykonać? Utrzymująca się lub nawracająca podwyższona temperatura ciała może być sygnałem białaczki.
 To nowotwór krwi, który powoduje, że organizm wytwarza nieprawidłowe białe krwinki, osłabiając zdolności zwalczania infekcji przez układ odpornościowy.
 Budzisz się rano, spoglądasz na przedramię, a na nim widzisz sianiaka.
 Dość silne przekrwienie, najczęściej okrągłe.
 Może powstać na skutek uderzenia i pęknięcia naczyć krwionośnych, ale może też świadczyć o poważnej chorobie.
 Jeśli zauważysz, że na twoim ciele, często tuż po przebudzeniu, samoistnie pojawiają się siniaki - udaj się do lekarza Amerykańskie Centrum Leczenia Raka sugeruje, że sińce w nieoczywistych miejscach mogą sugerować białaczkę.
 Zmęczenie nie zawsze jest skutkiem niewyspania czy długotrwałego i silnego zdenerwowania.
 W tych przypadkach pomaga sen i odpoczynek od stresu.
 Czasem jednak zdarza się, że zmęczenie trwa miesiącami, w walce z nim nie pomaga nawet gorąca kąpiel, odpoczynek ani dwutygodniowy urlop.
 Odczuwasz zmęczenie, które trwa dłużej niż miesiąc? Skonsultuj się z lekarzem.
 Podobne objawy daje białaczka i rak tarczycy.
 Nowotwory jamy ustnej to choroby o których mówi się stosunkowo niewiele.
 Tymczasem są one bardzo poważne i dają objawy, które można pomylić z zakażeniem jamy ustnej.
 Zauważyłaś u siebie liczne afty, podrażenienia, bóle w jamie ustnej, krwawiące, trudne do wyleczenia ranki, obrzęk, czerwone plamki na jezyku lub dziąsłach i drętwienie szczęki? Nie bagatelizuj.
 Jeśli zmiany utrzymują się dłużej niż 2-4 tygodnie, mogą świadczyć o nowotworze jamy ustnej.

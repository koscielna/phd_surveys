 Wiele składników diety poza wartościami odżywczymi wykazuje również dodatkowe właściwości.
 Niektóre związki zawarte w owocach i warzywach, a także algach, są fitoaktywne.
 Do takich substancji należą polifenole, które – mimo że w układzie pokarmowym nie ulegają trawieniu – korzystnie wpływają na funkcjonowanie organizmu, a tym samym na nasze zdrowie.
 Produkty żywnościowe zawierające te ważne dla ciała składniki często określane są mianem superżywności, czyli żywności, która leczy.
 Zalety spożywania superżywności Warto na początku dodać, że mówiąc o superżywności, nie chodzi nam o suplementy wielowitaminowe czy wyciągi z egzotycznych roślin.
 Te nadzwyczajne produkty to warzywa i owoce oraz kilka innych popularnych składników diety.
 Zamiast zażywać tabletki, lepiej się skupić na konsumpcji superżywności, w której zgromadzone są w sposób naturalny znaczne ilości składników odżywczych.
 Do potraw, które przyrządzamy na co dzień, należy wprowadzić produkty, które zawierają najcenniejsze witaminy i minerały.
 Superżywność zawiera tak wiele ochronnych i zwiększających energię składników, że jedzenie ich przynosi większe korzyści niż zażywanie suplementów witaminowych.
 Najwspanialszą jej cechą jest naturalny bilans składników odżywczych, związków fitochemicznych i błonnika.
 Dzięki temu mają one zbawienny wpływ na organizm, zapobiegając wielu ciężkim chorobom.
 Dowiedziono, że spożywanie superżywności może przeciwdziałać nawet chorobom prostaty.
 Co jest superżywnością? Jakie produkty można więc nazwać superżywnością? Na podstawie zaleceń towarzystw naukowych zajmujących się żywieniem oraz artykułów opublikowanych w czasopismach naukowych poświęconych zdrowemu odżywianiu, opracowano listę najbardziej polecanych produktów, zawierających duże ilości składników korzystnie oddziałujących na zdrowie.
 Melanie Polk – dietetyk i specjalistka od chorób nowotworowych – uważa, że najwięcej takich produktów znajdziemy wśród roślin.
 To produkty roślinne zawierają olbrzymie ilości składników odżywczych, w tym witamin i składników mineralnych, pełniących niezwykle istotne funkcje w organizmach żywych.
 Przypomnijmy, że są one niezbędne do prawidłowego funkcjonowania wielu enzymów.
 Oto lista superproduktów polecanych przez Melanie Polk: brokuły, słodkie ziemniaki, różnego typu fasola, produkty razowe, czerwona papryka, pomidory (gotowane bądź surowe), jabłka, czarne porzeczki, owies, jagody, jarmuż (odmiana botaniczna kapusty), kiwi, kantalup (odmiana melona).
 Z kolei „The Environmental Nutrition Newsletter” jako najcenniejsze produkty z superżywności poleca: awokado, borówki, orzechy brazylijskie, brokuły, edamame (rodzaj fasoli), dynię piżmową, soczewicę komosę ryżową, siemię lniane (nasiona lnu), kiwi, cebulę, sardynki, pomidory, jogurty, jarmuż.
 Należy pamiętać, że lista produktów zaliczanych do superżywności to nie to samo co codzienne menu.
 Dieta powinna być zróżnicowana, tak aby dostarczała wszystkich niezbędnych składników odżywczych.
 Witaminy czy błonnik to bardzo ważne, lecz niejedyne składniki niezbędne dla organizmu człowieka.
 Aby dieta była właściwa, powinno się spożywać także pokarmy zawierające odpowiednie ilości węglowodanów czy kwasów tłuszczowych, które dostarczą ciału energii.
 Ważne jest jednak, aby nie spożywać produktów bogatych w nasycone kwasy tłuszczowe, które występują najczęściej w wyrobach cukierniczych i ciastkach.
 Należy również unikać tłuszczów występujących w tzw. konfiguracji trans, występujących masowo w fast foodach.
 Przyczyniają się one do wzrostu poziomu cholesterolu we krwi, zwiększając ryzko chorób układu krążenia.

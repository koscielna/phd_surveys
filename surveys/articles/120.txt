 Pigułki antykoncepcyjne słyną z wysokiej, niemal stuprocentowej skuteczności w zapobieganiu niechcianej ciąży, ale na tym nie kończą się ich zalety.
 Właściwie dobrane i stosowane zgodnie z zaleceniami, mogą złagodzić symptomy PMS i zmniejszyć ryzyko rozwoju niektórych schorzeń.
 Stosując tabletki hormonalne, możesz liczyć na krótsze i mniej obfite krwawienia miesięczne, ponadto rzadziej doświadczasz skurczów brzucha i uczucia dyskomfortu.
 Jakby tego było mało, jesteś w stanie „przełożyć” miesiączkę na później, np. gdy masz w planach wakacyjny wyjazd, ślub lub inne ważne wydarzenie.
 Gdy stosujesz doustną antykoncepcję, twój organizm regularnie otrzymuje dawkę estrogenu – hormonu, który powoduje spadek stężenia testosteronu (męskiego hormonu pobudzającego wydzielanie sebum przez skórę).
 W rezultacie twoja skóra jest mniej podatna na rozwój wyprysków trądzikowych i wygląda bardziej zdrowo.
 Dane statystyczne pokazują, że przyjmowanie tabletek antykoncepcyjnych może zmniejszać prawdopodobieństwo zachorowania na raka trzonu macicy lub raka jajników o 40 proc. po 1-5 latach stosowania pigułek, a nawet o 70 proc. po dwunastu latach terapii hormonalnej.
 Jest to zasługą redukcji liczby owulacji, które mogą wywoływać zmiany komórkowe w jajnikach, skutkujące rozwojem nowotworu.
 Zespół napięcia przedmiesiączkowego najczęściej objawia się skurczami brzucha, bólem piersi i wzdęciami – pojawienie się tych symptomów to wynik zmian hormonalnych w drugiej fazie cyklu miesiączkowego.
 Kobiety stosujące doustną antykoncepcję z reguły nie mają niemal żadnych problemów z PMS, ponieważ stała dawka hormonów zawartych w pigułkach chroni je przed wahaniami ich stężenia.
 Dodatkowe właściwości tabletek antykoncepcyjnych nie uszły uwadze lekarzy, którzy na co dzień mają styczność z pacjentkami doświadczającymi uciążliwych objawów PMS lub posiadającymi nasilone zmiany trądzikowe.
 Gdy inne metody łagodzenia przykrych symptomów zawodzą, nierzadko sugerują kobietom wypróbowanie pigułek – często z bardzo dobrym rezultatem.

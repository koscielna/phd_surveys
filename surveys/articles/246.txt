 Woda różni się od wszystkich innych związków występujących w naturze tym, że posiada pamięć.
 Pamięć wody, to zdolność przechowywania przez wodę częstotliwości elektromagnetycznych o długościach charakterystycznych dla substancji (zarówno toksycznych, jak i nietoksycznych), z którymi woda się zetknęła w przeszłości.
 Woda przenosi zapamiętane informacje przez określony sposób uporządkowania cząsteczek, niezależnie od tego, czy cząsteczki, z którymi się zetknęła w przeszłości jeszcze w niej istnieją, czy też woda została z nich oczyszczona.
 Cząsteczki wody mogą zmieniać swoje ustawienie na skutek wzajemnych oddziaływań elektromagnetycznych z cząsteczkami innych związków chemicznych, z którymi woda miała kontakt.
 Zmiana ustawienia cząsteczek, to zmieniona informacja i zmienione właściwości.
 Czynnikiem szkodliwym dla organizmu ludzkiego  nie jest sama substancja rozpuszczona w wodzie, której już tam nie ma, ale emitowane nadal niekorzystne częstotliwości elektromagnetyczne.
 Pamięć wody dotyczy również zdolności zapisywania w swojej strukturze częstotliwości elektromagnetycznych odpowiadających wszystkim impulsom dochodzącym z otaczającego nas świata (uczuciom, dźwiękom ,utworom muzycznym, obrazom, myślom i emocjom -  Masaru Emoto).
 Pamięć wody znajduje się w strukturach krystalicznych.
 (Iwan Nieumywakin.Woda utleniona na straży zdrowia.2008.str.113).
 Woda zawarta w świeżo wyciśniętych sokach działa odmładzająco i leczniczo.
 Posiadając pamięć, może działać jak lek homeopatyczny.
 Okazuje się, że największą skuteczność leczniczą posiada woda o najmniejszej zawartości substancji czynnej.
 Dr Lee H.Lorenzen, biochemik z USA był jednym z pierwszych, który udowodnił, że woda  może przenieść posiadane informacje do organizmu człowieka.
 Jego żona ciężko zachorowała, w końcowym stadium choroby nie mogła nawet przyjmować posiłków.
 Wtedy dr Lorenzen wpadł na pomysł, że będzie jej podawał wodę, w której zakoduje drgania posiłków narodu Hunzów.
 Informacje z tego pożywienia przenosił biorezonansem do wody, którą następnie podawał  żonie.
 Po tygodniu kobieta zaczęła w miarę normalnie funkcjonować, a po miesiącu była całkowicie zdrowa.
 W 1986 roku dr Lorenzen opatentował tę technikę.
 Obecnie ta właśnie metoda jest wykorzystywana do produkcji leków homeopatycznych.
 Pamięć wody jest wykorzystywana przez homeopatię i ma również swoją negatywną stronę.
 Dotyczy to przypadku, kiedy ujęcie wody pitnej znajduje się w rzece, którą płyną ścieki, zawierające różnego rodzaju toksyczne związki.
 Oczyszczenie chemiczne takiej wody niewiele daje, gdyż woda ta po prostu pamięta.
 (ponadto, toksyczne związki spożywają ryby, a po ich zjedzeniu – człowiek, ale to inny temat).
 Wykonano wiele różnego typu doświadczeń mających potwierdzić fakt posiadania przez wodę pamięci.
 Poniżej przedstawiam jedno nich.
 Wodę zawierającą 1%-owy roztwór chlorku sodu (NaCl = sól kuchenna) oddzielono płytką porcelanową od naczynia z czystą wodą i pozostawiono na 36 godzin.
 Później wodę czystą zamrożono, a kryształki lodu poddano badaniom na spektrofotometrze.
 Wyniki były szokujące, gdyż kryształki czystej wody wykazały obecność NaCl, natomiast badania chemiczne nie wykazały obecności chlorku sodu w naczyniu z czysta wodą.
 WNIOSEK ! Woda zawierająca 1%-owy roztwór chlorku sodu przekazała odpowiednią informację o swoim składzie poprzez układ drgań (fal elektromagnetycznych) do naczynia z czystą wodą, która w ten sposób nabyła cechy roztworu NaCl.
 (www.eioba.pl/a83793 Pamięć wody-fakt czy bzdura?) Podczas uboju zwierząt, na krótko przed śmiercią ich organizm wytwarza całą masę hormonów „faszerujących” komórki organizmu potwornym stresem.
 Stan psychiczny zwierząt  zostaje dokładnie zerejestrowany w strukturach wodnych.
 W rezultacie otrzymujemy mięso z trwałym śladem agonii.
 Teraz „faszerujemy” się tym wszystkim MY, ludzie i struktury wodne naszego organizmu otrzymują o tym odpowiednią informację.
 A później zaczynają się różnego rodzaju choroby.
 (www.eioba.pl/a83793 Pamięć wody – fakt czy bzdura)     Cząsteczka wody jest dipolem czyli cząsteczką polarną (asymetryczną), ponieważ środek ładunków dodatnich nie pokrywa się ze środkiem ładunków ujemnych.
 To powoduje, że cząsteczki wody łatwo ulegają asocjacji czyli łączeniu się w większe agregaty, które otaczają jony (kationy i aniony) toksycznych substancji.
 Agregaty te mają własną częstotliwość rezonansową i zdolność do rozpadania się (pękają delikatne wiązania wodorowe /protonowe/), w wyniku czego dochodzi do uwalniania i usuwania z komórek organizmu szkodliwych substancji.
 Wodne agregaty po usunięciu cząsteczek, wokół których powstały istnieją nadal.
 Na tym polega pamięć wody, nie wystarczy usunięcie danego toksycznego związku (np. ołowiu, kadmu, rtęci czy aluminium), ale musi zostać również usunięta o nim informacja.
 Okazuje się, że woda zatruta różnymi szkodliwymi substancjami po oczyszczeniu chemicznym i usunięciu bakterii, a nawet dwukrotnej destylacji wysyła nadal fale elektromagnetyczne o długościach charakterystycznych dla usuniętych substancji.
 Fale te dla zdrowia mogą być szkodliwe.
 Również według dr Grażyny Pająk woda oczyszczona z najbardziej szkodliwych pierwiastków nadal zachowuje informację o nich, jednak usuwanie zanieczyszczeń mechanicznych i chemicznych wody, to za mało, niezbędne jest przywrócenie jej właściwej informacji elektromagnetycznej jaką posiada w naturze.
 Filtry węglowo–ceramiczne czy osmotyczne, destylatory używane do produkcji wody czynią ją czystą, ale zarazem martwą energetycznie.
 Woda taka nie wykazuje jakichkolwiek uporządkowanych struktur i po zamrożeniu wygląda pod mikroskopem jak brzydka niekształtna masa, natomiast woda żywa energetycznie posiada strukturę krystaliczną (heksagonalną).
 Po zamrożeniu tworzy przepiękne kryształy (zawsze na bazie sześciokąta), przypominające płatki śniegowe.
 Woda naturalna o pierwotnej strukturze, to taka, jaką spotykamy w lodowcach, dziewiczych rzekach, czystych jeziorach.
 W środowisku naturalnym podlega ona oddziaływaniu pola magnetycznego Ziemi, promieni słonecznych, oddziaływaniu Księżyca, przepływa przez różne rodzaje skał, tańczy z wiatrem, faluje, pieni się, wpada w wiry, opada kaskadami.
 Ciągły ruch i wszystkie inne czynniki wpływają na jej odnowienie.
 Wówczas  niesie w sobie olbrzymie pokłady energii i informacji, które przekazuje wszystkim organizmom żywym.
 Jak udowodnił Masaru Emoto, zamrażanie wody wodociągowej przy obecnym jej obciążeniu (czyli zatoksycznieniu),  z reguły nie przywraca jej pierwotnej struktury.
 Jednak doświadczenia wykazały, że niewłaściwe informacje zapisane w wodzie można skasować za pomocą urządzeń, które wykorzystując prawa natury przywracają wodzie jej pierwotną strukturę.
 W efekcie oczyszczona, a następnie ożywiona (witalizowana) woda z sieci odzyskuje właściwości wody źródlanej.
 Woda ożywiona technologią Grandera ma wykasowane z pamięci wszystkie szkodliwe informacje, dotyczące zarówno przebytych chorób, jak i zetknięcia się w przeszłości z różnymi toksycznymi substancjami.
 (www.grander.obywatel.pl)       Do urządzeń z tak zwanej „górnej półki” należałoby zaliczyć jonizator LeveLuk SD 501 wytwarzany w Japonii, który produkuje m.in. wodę żywą zawierającą asocjaty zbudowane z 5-6 cząsteczek wody.
 Woda ta (strukturyzowana) jest wartościowa energetycznie, posiada sześcioramienną (heksagonalną) strukturę krystaliczną, a woda o takiej strukturze ma znacznie obniżone napięcie powierzchniowe, dzięki czemu szybciej dostarcza składniki odżywcze do komórek organizmu oraz szybciej usuwa z nich toksyny i wolne rodniki eliminowane przez produkowane przez jonizator aktywne jony wodoru (elektrony redukujące).
 Transport składników odżywczych do i z komórek odbywa się w wyniku przenikania ich przez półprzepuszczalną membranę komórki.
 Zbyt wysokie napięcie powierzchniowe cząsteczki wody pitnej powoduje odwodnienie, co zwykle jest przyczyną zatrucia.
 Rumuński naukowiec Henri Coanda badając wodę plemienia Hunzów odkrył, że ma ona znacznie obniżone  napięcie powierzchniowe.

from .base import *
import json
from django.core.exceptions import ImproperlyConfigured

with open(os.environ.get('SURVEYS_CONFIG')) as f:
    configs = json.loads(f.read())


def get_env_var(setting, configs=configs):
    try:
        val = configs[setting]
        if val == 'True':
            val = True
        elif val == 'False':
            val = False
        return val
    except KeyError:
        error_msg = "ImproperlyConfigured: Set {0} environment variable".format(setting)
        raise ImproperlyConfigured(error_msg)

SECRET_KEY = get_env_var("SECRET_KEY")

DEBUG = True

ALLOWED_HOSTS = ['127.0.0.1', 'localhost', 'testserver']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'cred_surveys',
        'USER': 'ola',
        'PASSWORD': '',
        'HOST': 'localhost',
        'PORT': '',
    }
}
